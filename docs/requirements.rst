Requirements
============
Platform is essentially just a series of components that work with Laravel 5.3.
So the requirements are virtually the same.
However some components may require dependencies with their own set of minimum requirements.::

    PHP >= 5.6.4
    OpenSSL PHP Extension
    PDO PHP Extension
    Mbstring PHP Extension
    Tokenizer PHP Extension
    XML PHP Extension
