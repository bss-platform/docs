Adapter
=======

GCM
---
Returns
::

   Core\Push\Services\Adapters\Gcm

Arguments
::

Example
::

   $adapter = CorePush::gcmApp();


Apns
----
Returns
::

   Core\Push\Services\Adapters\Apns

Arguments
::

Example
::

   $adapter = CorePush::apnsApp();

OneSignal
---------
Returns
::

   Core\Push\Services\Adapters\OneSignal

Arguments
::

Example
::

   $adapter = CorePush::oneSignalApp();

Parse
-----
Returns
::

   Core\Push\Services\Adapters\Parse

Arguments
::

Example
::

   $adapter = CorePush::parseApp();
