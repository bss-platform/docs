Register user device token
==========================

Returns
::

   Core\Push\Repositories\Userdevices\UserdevicesRepositoryInterface $instance

Arguments
::

    createNew
        Uuid user_id
        String device_type : android/ios
        String device_token
        String time_zone
        Double latitude
        Double longitude
        String version
        Date last_active
        0/1 is_login
        String app_version
        String app_identifier
        Date expired_at

Example
::

    $input = array(
        'device_type' => 'android',
        'device_token' => 'd1dOvHUoQQg:APA91bH7GtrJlx-Y_yMBxB6CH22qDNvDYDbC1qMlGfAzbtuPGx8RRExVOyyd28a4_nFv9WTo9OQ-fsE1Jt4SZVyJ-PTyaAskhQsDI64T6zilc6MvvUl0OdCzrqUiHYhDgiOgXZTBbIN5',
        'location' =>array(
            'latitude' => 10.7992916,
            'longitude' => 106.6430301
        ),
       'time_zone' => 'Asia/Jakarta',
       'version' => '1.1.0',
       'is_login' => 1,
       'app_version' => 'v1.1.0',
       'app_identifier' => 'BSSCORE'
    );

     $data = array(
        'user_id' => xxx,
        'device_type' => array_get($input, 'device_type', 'android'),
        'device_token' => array_get($input, 'device_token'),
        'time_zone' => array_get($input, 'time_zone'),
        'version' => array_get($input, 'version'),
        'last_active' => Carbon::now(),
        'is_login' => array_get($input, 'is_login', 1),
        'app_version' => array_get($input, 'app_version'),
        'app_identifier' => array_get($input, 'app_identifier'),
        'expired_at' => Carbon::now()->addMonths(2),
    );
    $location = array_get($input, 'location');
    if (!empty($location)) {
        $data['latitude'] = array_get($location, 'latitude');
        $data['longitude'] = array_get($location, 'longitude');
    }

    if (adapter  === 'parse') {
        //require to register device token Parse system
        $this->installation($data);
    }

    $this->userdevicesRepository->updateOrCreate([
        'user_id' => 1,
        'device_type' => array_get($data, 'device_type'),
        'device_token' => array_get($data, 'device_token')
    ], $data);


Register user device token Parse system
---------------------------------------
::

        /*
        *
        * @var $data
        * Install parse (adapter)
        *
        * */
        protected function installation($data)
        {
            //only Parse begin
            $parseInstallation = new ParseInstallation();
            $parseInstallation->set('deviceType', $data['device_type']);
            $parseInstallation->set('deviceToken', $data['device_token']);
            $parseInstallation->set('timeZone', $data['time_zone']);

            $configPushDriver = config('core.push.parse));
            $config = array_get($configPushDriver, config('core.push.environment'));
            $parseInstallation->set('GCMSenderId', array_get($config, 'senderId'));

            if ($data['device_type'] === App::ANDROID) {
                $parseInstallation->set('pushType', 'gcm');
            }
            if (array_get($data, "latitude", null) !== null) {
                $point = new ParseGeoPoint(
                    array_get($data, 'latitude'),
                    array_get($data, 'longitude')
                );
                //This point is then stored in the object as a regular field
                $parseInstallation->set("location", $point);
            }

            $parseInstallation->save();
            //only Parse end
        }
