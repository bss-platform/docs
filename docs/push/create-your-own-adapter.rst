Create your own Adapter
=======================

Defined Namespace
-----------------
::

    namespace Core\Push\Services\Adapters;

Defined Interface
-----------------
``Core\Push\Services\Adapters\AdapterInterface``

::

    interface AdapterInterface
    {
        /**
         * Push.
         *
         * @param \Core\Push\Services\Model\PushInterface $push Push
         *
         * @return \Core\Push\Services\Collection\DeviceCollection
         */
        public function push(PushInterface $push);

        /**
         * Supports.
         *
         * @param string $token Token
         *
         * @return boolean
         */
        public function supports($token);

        /**
         * Get defined parameters.
         *
         * @return array
         */
        public function getDefinedParameters();

        /**
         * Get default parameters.
         *
         * @return array
         */
        public function getDefaultParameters();

        /**
         * Get required parameters.
         *
         * @return array
         */
        public function getRequiredParameters();
    }
Defined Abstract BaseAdapter
----------------------------
``Core\Push\Services\Adapters\BaseAdapter``
::

    abstract class BaseAdapter extends BaseParameteredModel implements AdapterInterface


Write your own adapter
----------------------
Example
::

    <?php namespace Core\Push\Services\Adapters;

    use Core\Push\Services\Exception\RuntimeException;
    use Core\Push\Services\Model\BaseOptionedModel;
    use Core\Push\Services\Model\PushInterface;
    use Core\Push\Services\Collection\DeviceCollection;
    use Core\Push\Services\Exception\PushException;

    use CoreOneSignalClient;
    use Core\Push\Services\Traits\MessageTrait;

    class OneSignal extends BaseAdapter
    {
        use MessageTrait;

        /**
         * {@inheritdoc}
         */
        public function supports($token)
        {
            return is_string($token) && $token != '';
        }

        /**
         * {@inheritdoc}
         *
         * @throws \Core\Push\Services\Exception\PushException
         */
        public function push(PushInterface $push)
        {
            $pushedDevices = new DeviceCollection();
            $tokens = array_chunk($push->getDevices()->getTokens(), 10);

            foreach ($tokens as $tokensRange) {
                $data = $this->getServiceMessage($tokensRange, $push->getMessage(), $this->os);
                try {
                    $this->response = CoreOneSignalClient::__get('notifications')->add($data->toArray());

                } catch (RuntimeException $e) {

                    throw new PushException($e->getMessage());
                }

                foreach ($tokensRange as $token) {
                    $pushedDevices->add($push->getDevices()->get($token));
                }
            }
            return $pushedDevices;
        }


        /**
         * Get service message from origin.
         *
         * @param array $tokens Tokens
         * @param BaseOptionedModel|\Core\Push\Services\Model\MessageInterface $message Message
         * @param string $os
         * @return mixed
         */
        public function getServiceMessage(array $tokens, BaseOptionedModel $message, $os)
        {
            return $this->getOneSignalServiceMessage($tokens, $message, $os);
        }


        /**
         * {@inheritdoc}
         */
        public function getDefinedParameters()
        {
            return [

            ];
        }

        /**
         * {@inheritdoc}
         */
        public function getDefaultParameters()
        {
            return [
                'appId',
                'restApiKey',
                'userOauthKey'
            ];
        }

        /**
         * {@inheritdoc}
         */
        public function getRequiredParameters()
        {
            return [
                'appId',
                'restApiKey',
                'userOauthKey'
            ];
        }
    }


Use your own adapter
--------------------
Returns
::

    Core\Push\Services\Adapters\{ucFirst($adapterName)}

Arguments
::
     String $adapterName

Example
::

    $adapterName = 'OneSignal';
    $adapter = CorePush::adapterApp(lcfirst($adapterName));


