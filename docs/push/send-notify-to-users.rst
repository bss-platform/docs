Send notify to users
====================

Config Eloquent User
--------------------

- Eloquent User should exetends  ``\Core\Push\Services\Eloquents\User`` class
- Example
::

    <?php

    namespace Core\Demo\Eloquents;
    class User extends \Core\Push\Services\Eloquents\User
    {

        /**
         * Route notifications for the mail channel.
         *
         * @return string
         */
        public function routeNotificationForMail()
        {
            return $this->email;
        }
    }


Send notify to users
--------------------

Returns
::

   void

Arguments
::

    Core\Push\Services\Eloquents\User $instance
    Core\Push\Services\Notifications\{Adapter}Notify $instance

Example
::

    $users = $this->eloquentUser->where([
        'id' => 1
    ])->get();
    Notification::send($users, $notify);






