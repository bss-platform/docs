Install
=======

Update composer.json
--------------------
::

    "repositories": [
        {
            "url": "git@bitbucket.org:bss-vendor/push.git",
            "type": "git"
        }
    ],
    "require": {
        "core/push": "dev-master"
    }

Dependency service vendor to use
--------------------------------
::

    "require": {
        "norkunas/onesignal-php-api": "dev-master",
        "twilio/sdk": "5.*",
        "nexmo/client": "^0.4.0",
        "plivo/plivo-php": "^1.1",
        "parse/php-sdk" : "1.2.*"
    }

Update composer
---------------
::

    composer update

Publish vendor
--------------
::

    php artisan vendor:publish --provider="Core\Push\Providers\PushServiceProvider" --tag="config"


==> Copied File [/extensions/core/push/src/config/config.php] To [/config/core.push.php]


Environment
-----------
::

    #############CORE NOTIFICATION #############
    #config/core.push.php
    CORE_PUSH_ENVIRONMENT=development

    ##PARSE##
    CORE_PUSH_PARSE_APP_ID=1234
    CORE_PUSH_PARSE_REST_KEY=1234
    CORE_PUSH_PARSE_MASTER_KEY=1234
    CORE_PUSH_PARSE_SERVER_URL=http://192.168.99.100:1337
    CORE_PUSH_PARSE_MOUNT_PATH=/parse
    ##PARSE##

    ##APNS##
    CORE_PUSH_APNS_CERTIFICATE=
    CORE_PUSH_APNS_PASS_PHRASE=
    ##APNS##

    ##GCM##
    CORE_PUSH_GCM_SENDER_ID=291367353844
    CORE_PUSH_GMC_API_KEY=AAAAQ9bY_fQ:APA91bEXd3kPUVd_CI18-Satok4PiG_qM3GQr6ju-pgXA-RJnNhSPrlh2FW86m-RwVMPsN01ywmnxeX4wfeyNQOxpRbt8EgwA7oUA-O2XCc9LSywqdBBIAwnOrwbIlSSnw9AXCBtoask
    ##GCM##

    ##ONESIGNAL##
    CORE_PUSH_ONESIGNAL_APP_ID=25a6f59a-752e-47e5-9f28-a97780c2b55c
    CORE_PUSH_ONESIGNAL_REST_API_KEY=ZGZiNjhjZWQtNGFiNS00MDI1LWE0NGItN2RlZWFiYmYyMDNi
    CORE_PUSH_ONESIGNAL_USER_OAUTH_KEY=
    ##ONESIGNAL##

    ##TWILIO##
    CORE_PUSH_TWILIO_ACCOUNT_SID=AC3c5569a53af83a14c128898a88de72de
    CORE_PUSH_TWILIO_AUTH_TOKEN=5a04665e4264368d6eba38f734449420
    CORE_PUSH_TWILIO_FROM=+17742216319
    ##TWILIO##

    ##PLIVO##
    CORE_PUSH_PLIVO_ACCOUNT_SID=AC3c5569a53af83a14c128898a88de72de
    CORE_PUSH_PLIVO_AUTH_TOKEN=5a04665e4264368d6eba38f734449420
    CORE_PUSH_PLIVO_FROM=+17742216319
    ##PLIVO##

    ##NEXMO##
    CORE_PUSH_NEXMO_KEY=9179556c
    CORE_PUSH_NEXMO_SECRET=693c229db3ccea1f
    CORE_PUSH_NEXMO_FROM=NEXMO
    ##NEXMO##

    ##AUTHY##
    CORE_PUSH_AUTHY_API_KEY=tc4tYAE5Cp7GeFqDQrrz8p64FV11q4J4
    ##AUTHY##


    #############CORE NOTIFICATION #############

Update providers
----------------
Register the PushNotification service provider by adding it to the providers array  in the ``app/config/app.php`` file.
::

    "providers" => [
        ...

        //Push Notification
        Core\Push\Services\Services\PushServiceProvider::class,
        Core\Push\Services\Services\Parse\ParsePushServiceProvider::class,
        Core\Push\Services\Services\OneSignal\OneSignalPushServiceProvider::class,

        //Push SMS
        Core\Push\Services\Services\Twilio\TwilioServiceProvider::class,
        Core\Push\Services\Services\Plivo\PlivoServiceProvider::class,
        Core\Push\Services\Services\Nexmo\NexmoServiceProvider::class,


Setup queue worker
------------------
- Development or localhost

::

    php artisan queue:listen

- Production or demo

Should setup supervisord

::

    [program:laravel-worker]
    process_name=%(program_name)s_%(process_num)02d
    command=php /data/www/artisan queue:work  --sleep=3 --tries=3  --timeout=120 --force
    autostart=true
    autorestart=true
    user=root
    numprocs=8
    redirect_stderr=true
    stdout_logfile=/var/log/laravel-worker.log
