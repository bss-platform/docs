Device
======

Device
------
Returns
::

   Core\Push\Services\Model\Device

Arguments
::

    String device token

Example
::

    CorePush::Device($deviceToken, []),

Device Collection
-----------------
Returns
::

   Core\Push\Services\Collection\DeviceCollection

Arguments
::

    Array Core\Push\Services\Model\Device

Example
::

    $deviceList = [
        CorePush::Device($deviceToken, []),
        CorePush::Device($deviceToken2, []),
        ...
    ];
    $devices = CorePush::DeviceCollection($deviceList);

