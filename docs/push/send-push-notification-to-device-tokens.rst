Send Push Notification to device tokens
=======================================

Adpater
-------
- GCM
::

    $adapter = CorePush::gcmApp();

- Apns
::

    $adapter = CorePush::apnsApp();

- OneSignal
::

    $adapter = CorePush::android()->oneSignalApp();
    $adapter = CorePush::iOS()->oneSignalApp();

- Parse
::

    $adapter = CorePush::android()->parseApp();
    $adapter = CorePush::iOS()->parseApp();


Device Collection
-----------------
::

    $deviceCollection = [];
    foreach ($deviceTokens as $token) {
        $deviceList[] = CorePush::Device($token);
    }
    $deviceCollection =  CorePush::DeviceCollection($deviceList);

Send direct push notification
-----------------------------
::

    $collection = $adapter->to($deviceCollection)->send($message);

    foreach ($collection->pushManager as $push) {
        $response = $push->getAdapter()->getResponse();
        \Log::info([$response]);
    }

Send to message queue
---------------------
- Create a push job
::

    class SendPushNotificationJob implements ShouldQueue
    {
        use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


        /**
         * Create a new App instance.
         * @var  App $app
         *
         */
        protected $app;

        /**
         * Create a new MessageInterface instance.
         * @var  MessageInterface $adapter
         *
         */
        protected $message;

        /**
         * Create a new DeviceCollection instance.
         * @var  DeviceCollection $devices
         *
         */
        protected $deviceCollection;

        /**
         * Create a new job instance.
         * @var  App $app
         * @var  DeviceCollection $devices
         * @var  MessageInterface $message
         *
         * @return void
         */
        public function __construct(App $app, DeviceCollection $deviceCollection, MessageInterface $message)
        {
            $this->app = $app;
            $this->deviceCollection = $deviceCollection;
            $this->message = $message;
        }

        /**
         * Execute the job.
         *
         * @return void
         */
        public function handle()
        {
            $collection = $this->app->to($this->deviceCollection)->send($this->message);
            // get response for each device push
            foreach ($collection->pushManager as $push) {
                $response = $push->getAdapter()->getResponse();
                \Log::info([$response]);
            }

        }

        /**
         * The job failed to process.
         *
         * @param \Exception $exception
         * @return void
         */
        public function failed(\Exception $exception)
        {
            // Send user notification of failure, etc...
            \Log::error($exception);
        }
    }

- Dispatch a job to a message queue
::

    $job = (new SendPushNotificationJob($adapter,$deviceCollection,$message));
    dispatch($job);


- Dispatch a delayed job to a message queue
::

    $job = (new SendPushNotificationJob($adapter,$deviceCollection,$message)->delay(Carbon::now()->addMinutes(10));
    dispatch($job);
