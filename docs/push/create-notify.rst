Create Notify
=============


GCM/Apns
--------
Returns
::

   Core\Push\Services\Notifications\EmailNotify

Arguments
::
     Core\Push\Repositories\Push\PushRepositoryInterface $instance

Example
::

    $payload = [
        "alert" => "Default : Welcome to BSSCORE NOTIFICATION @2017",
        "message" => "Default : Thanks for choosing BSSCORE NOTIFICATION. We're happy to assist you on your Push journey!",
        "title" => "Default : Welcome to BSSCORE NOTIFICATION @2017",
        "style" => "picture",
        "picture" => "https://www.askideas.com/media/13/Welcome-Picture.jpg",
        "summaryText" => "Default : Thanks for choosing BSSCORE NOTIFICATION. We're happy to assist you on your Push journey!"
    ];
    $message = CorePush::Message($payload["message"], $payload);
    $title = $message->getText();
    $title = is_array($title) ? array_shift($title) : $title;
    $title = str_limit($title, 100);
    $messageModel = $this->pushRepository->createNew([
        'type' => 'notification',
        'adapter' => 'default,
        'title' => $title
    ]);
    $messageModel->setPayload([
        'text' => $message->getText(),
        'options' => $message->getOptions()
    ]);
    $messageModel->save();

    $notify = new DefaultNotify($messageModel);

OneSignal
---------
Returns
::

   Core\Push\Services\Notifications\OneSignalNotify

Arguments
::

     Core\Push\Repositories\Push\PushRepositoryInterface $instance

Example
::

    $payload = [
        "contents" => [
            "en" => "OneSignal : Thanks for choosing BSSCORE NOTIFICATION. We're happy to assist you on your Push journey!"
        ],
        "headings" => [
            "en" => "OneSignal : Welcome to BSSCORE NOTIFICATION @2017"
        ],
        "subtitle" => [
            "en" => "OneSignal : Welcome to BSSCORE NOTIFICATION @2017"
        ]
    ];
    $message = CorePush::Message($payload["contents"], $payload);
    $title = $message->getText();
    $title = is_array($title) ? array_shift($title) : $title;
    $title = str_limit($title, 100);
    $messageModel = $this->pushRepository->createNew([
        'type' => 'notification',
        'adapter' => 'neSignal,o
        'title' => $title
    ]);
    $messageModel->setPayload([
        'text' => $message->getText(),
        'options' => $message->getOptions()
    ]);
    $messageModel->save();

    $notify = new OneSignalNotify($messageModel);

Parse
-----
Returns
::

   Core\Push\Services\Notifications\ParseNotify

Arguments
::

    Core\Push\Repositories\Push\PushRepositoryInterface $instance

Example
::

    $payload = [
        'alert' => 'Parse : Welcome to BSSCORE NOTIFICATION @2017',
        'badge' => 'Increment', //(iOS only)
        'sound' => 'cheering.caf', //(iOS only)
        'content_available' => [], //(iOS only)
        'title' => 'Parse : Welcome to BSSCORE NOTIFICATION @2017', //(Android only)
        'category' => 1,
        'uri' => '' //(Android only)
    ];
    $message = CorePush::Message($payload["alert"], $payload);
    $title = $message->getText();
    $title = is_array($title) ? array_shift($title) : $title;
    $title = str_limit($title, 100);
    $messageModel = $this->pushRepository->createNew([
        'type' => 'notification',
        'adapter' => 'parse,
        'title' => $title
    ]);
    $messageModel->setPayload([
        'text' => $message->getText(),
        'options' => $message->getOptions()
    ]);
    $messageModel->save();

    $notify = new ParseNotify($messageModel);

Plivo
-----
Returns
::

   Core\Push\Services\Notifications\PlivoNotify

Arguments
::

     Core\Push\Repositories\Push\PushRepositoryInterface $instance

Example
::

    $title = 'Plivo : Welcome to BSSCORE NOTIFICATION @2017';
    $text = 'Plivo : Welcome to BSSCORE NOTIFICATION @2017';
    $messageModel = $this->pushRepository->createNew([
        'type' => 'sms',
        'adapter' => 'plivo',
        'title' => $title
    ]);
    $messageModel->setPayload([
        'text' => $text,
        'options' => []
    ]);
    $messageModel->save();

    $notify = new PlivoNotify($messageModel);

Twilio
------
Returns
::

   Core\Push\Services\Notifications\TwilioNotify

Arguments
::

     Core\Push\Repositories\Push\PushRepositoryInterface $instance

Example
::

    $title = 'Twilio : Welcome to BSSCORE NOTIFICATION @2017';
    $text = 'Twilio : Welcome to BSSCORE NOTIFICATION @2017';
    $messageModel = $this->pushRepository->createNew([
        'type' => 'sms',
        'adapter' => 'twilio',
        'title' => $title
    ]);
    $messageModel->setPayload([
        'text' => $text,
        'options' => []
    ]);
    $messageModel->save();

    $notify = new TwilioNotify($messageModel);

Nexmo
-----
Returns
::

   Core\Push\Services\Notifications\NexmoNotify

Arguments
::

     Core\Push\Repositories\Push\PushRepositoryInterface $instance

Example
::

    $title = 'Nexmo : Welcome to BSSCORE NOTIFICATION @2017';
    $text = 'Nexmo : Welcome to BSSCORE NOTIFICATION @2017';
    $messageModel = $this->pushRepository->createNew([
        'type' => 'sms',
        'adapter' => 'nexmo',
        'title' => $title
    ]);
    $messageModel->setPayload([
        'text' => $text,
        'options' => []
    ]);
    $messageModel->save();

    $notify = new NexmoNotify($message);

Email
-----
Returns
::

   Core\Push\Services\Notifications\EmailNotify

Arguments
::

     Core\Push\Repositories\Push\PushRepositoryInterface $instance

Example
::

    $adapter = 'gmail';
    $title = 'Email : Welcome to BSSCORE NOTIFICATION @2017';
    $text = 'Email : Welcome to BSSCORE NOTIFICATION @2017';
    $messageModel = $this->pushRepository->createNew([
        'type' => 'email',
        'adapter' => 'gmail',
        'title' => $title
    ]);
    $messageModel->setPayload([
        'text' => $text,
        'options' => [
            'is_mail_markdown' => true,
            'mail_view' => 'core/demo::email.notify',
            'url' => 'https://beesightsoft.com',
            'title' => $title,
            'background' => 'https://beesightsoft.com/wp-content/themes/beesightsoft/img/team.jpg?ver=1',
            'content' => 'Founded since 2011 by young and passionate technologists. Beesight Soft has started by innovative passion "together makes perfect". Since the, Beesight Soft has been initially successful in providing high quality outsourcing services, creating reputation for many domestic and foreign enterprises. Beesight Soft will grow with continuous efforts to explore the needs and build the best solutions for your business within reasonable budget.'
        ]
    ]);
    $messageModel->save();

    $notify = new EmailNotify($messageModel);


