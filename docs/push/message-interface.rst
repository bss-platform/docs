Message Interface
=================

GCM
---
Returns
::

   Core\Push\Services\Model\Message

Arguments
::

Example
::

    //define payload
    $payload = [
        "alert" => "Welcome to BSSCORE NOTIFICATION @2017",
        "message" => "Thanks for choosing BSSCORE NOTIFICATION. We're happy to assist you on your MongoDB journey!",
        "title" => "Welcome to BSSCORE NOTIFICATION @2017",
        "style" => "picture",
        "picture" => "https://www.askideas.com/media/13/Welcome-Picture.jpg",
        "summaryText" => "Thanks for choosing BSSCORE NOTIFICATION. We're happy to assist you on your MongoDB journey!"
    ];
    $message = CorePush::Message($payload['message'], $payload);


 [GCM](https://developers.google.com/cloud-messaging/concept-options#options)

Apns
----
Returns
::

   Core\Push\Services\Model\Message

Arguments
::

Example
::

    //define payload
    $payload = [
        "alert" => "Welcome to BSSCORE NOTIFICATION @2017",
        "message" => "Thanks for choosing BSSCORE NOTIFICATION. We're happy to assist you on your MongoDB journey!",
        "title" => "Welcome to BSSCORE NOTIFICATION @2017",
        "style" => "picture",
        "picture" => "https://www.askideas.com/media/13/Welcome-Picture.jpg",
        "summaryText" => "Thanks for choosing BSSCORE NOTIFICATION. We're happy to assist you on your MongoDB journey!"
    ];
    $message = CorePush::Message($payload['message'], $payload);

[APNS](https://developer.apple.com/library/content/documentation/NetworkingInternet/Conceptual/RemoteNotificationsPG/PayloadKeyReference.html#//apple_ref/doc/uid/TP40008194-CH17-SW1)

OneSignal
---------
Returns
::

   Core\Push\Services\Model\Message

Arguments
::

Example
::

    $payload = [
        "contents" => [
            "en" => "OneSignal : Thanks for choosing BSSCORE NOTIFICATION. We're happy to assist you on your Push journey!"
        ],
        "headings" => [
            "en" => "OneSignal : Welcome to BSSCORE NOTIFICATION @2017"
        ],
        "subtitle" => [
            "en" => "OneSignal : Welcome to BSSCORE NOTIFICATION @2017"
        ]
    ];
    $message = CorePush::Message($payload["contents"], $payload);
    $message->setOptions($data);
    $message->setOption($key,$value);


[OneSignal](https://documentation.onesignal.com/v3.0/reference#create-notification)

Parse
-----
Returns
::

   Core\Push\Services\Model\Message

Arguments
::

Example
::

    $payload = [
        'alert' => 'Parse : Welcome to BSSCORE NOTIFICATION @2017',
        'badge' => 'Increment', //(iOS only)
        'sound' => 'cheering.caf', //(iOS only)
        'content_available' => [], //(iOS only)
        'title' => 'Parse : Welcome to BSSCORE NOTIFICATION @2017', //(Android only)
        'category' => 1,
        'uri' => '' //(Android only)
    ];
    $message = CorePush::Message($payload["alert"], $payload);
    $message->setOptions($data);
    $message->setOption($key,$value);

[Parse](http://docs.parseplatform.org/rest/guide/#sending-pushes)




