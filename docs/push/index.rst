Push Notification
=================
.. image:: ../images/corepush.png
.. toctree::
    :maxdepth: 4
    :glob:

    install
    adapter
    create-your-own-adapter
    message-interface
    device
    register-user-device-token
    send-push-notification-to-device-tokens
    create-notify
    send-notify-to-users
    user-push-histories
