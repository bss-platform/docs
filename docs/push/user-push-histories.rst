User push histories
===================

Get user push histories
-----------------------
Example
::

    $user = $this->eloquentUser->find(1);
    $histories = $user->userPushes;

Get user read push histories
----------------------------
Example
::

    $user = $this->eloquentUser->find(1);
    $histories = $user->readUserPushes;

Get user unread push histories
------------------------------
Example
::

    $user = $this->eloquentUser->find(1);
    $histories = $user->unreadUserPushes;

