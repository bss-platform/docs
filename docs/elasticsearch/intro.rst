Intro
=====
What's ElasticSearch?
---------------------

The first thing to understand about ElasticSearch is: what exactly is ElasticSearch?

ES is a document oriented database.
This means that it's all about storing and retrieving documents.
In ES all documents are represented in JSON format.
It is built on Lucene a information retrieval software that has more than 15 years of experience on full text indexing and searching.

If you have ever used MongoDB probably you'll be familiarized with a JSON storage service.
But what makes ES really special is its information retrieval nature.
This combination of storage and querying/aggregation services makes ES really special and distant from the "document storage only" tools.

Given its nature another important thing to know is when to use ElasticSearch.
It won't be a good idea to switch your SQL databases for ES.
They have different purposes and each one has its benefits and drawbacks.
Some of the use cases where ES excels are:

* Searching for pure text (textual search)
* Searching text and structured data (product search by name + properties)
* Data aggregation
* Geo Search
* JSON document storage

Basic Concepts
--------------

Let's take a look at the main concepts of ElasticSearch:

* **Cluster**: A set of Nodes (servers) that holds all the data.
*  **Node**: A single server that holds some data and participate on the cluster's indexing and querying.
*  **Index**: Forget SQL Indexes. Each ES Index is a set of Documents.
*  **Shards**: A subset of Documents of an Index. An Index can be divided in many shards.
*  **Type**: A definition of the schema of a Document inside of an Index (a Index can have more than one type assigned).
*  **Document**: A JSON object with some data. It's the basic information unit in ES.


Good and Bad of ElasticSearch
-----------------------------

First of all I'd like to talk about some of the reasons to consider using ES on your system.

The first thing is **speed**. ES is has very good performance. As I said before it's built on top of Lucene and the capability of spanning queries in parallel inside a cluster performs very well (more on that parallels in a latter post).

Other nice thing about ElasticSearch is that for it's queries the result can be ordered by Relevance. By default ES uses the TF/IDF similarity algorithm to calculate the relevance. If you don't know what is relevance take a look at this page in the ES documentation.

Last but not least, ES can be very useful to generate aggregate statistics and have very versatile Search API that can meet you requirements with little effort.

But there are some bad things too. There is no silver bullet! ElasticSearch is very powerful in searching and aggregating data, but if you have an environment of extremely writing operations, maybe ES won't be your best option (but you can at least give it a try, it doesn't do that bad).

Also, it doesn't have any kind of transactional operations. But as long as you don't rely on it as your primary data storage you should be fine.


Rest API
--------

ElasticSearch uses a REST API for searching and storing documents. Below is an example of indexing (storing) a document:
::

    $ curl -XPUT 'http://localhost:9200/blog/post/1' -d '{
        "author": "lucas",
        "tags": ["java", "web"],
        "title": "A Fancy Title",
        "context": "A nice post content..."
    }'

The ``http://localhost:9200/`` is the address of our ES node. Here we are creating a blog post in the index blog with a type post and an id = 1. Note that it's possible to use a POST instead of aPUT to automatically generate the document id. The document itself is a ordinary JSON document.

Now let's take a look at the request to retrieve a document:
::

    $ curl -XGET 'http://localhost:9200/blog/post/_search' -d '{
        "query" : {
            "term" : { "author" : "lucas" }
        }
    }'

It's very straightforward to understand what is being queried. A query can be send either by a GET or a POST. The blog defines which indexes we are querying and the post is the document type (it's an optional parameter). The _search are telling the action that we want to execute. The body of the query will determine the type of query and the parameters. In this case we are looking for any documents that the value of field author has the term lucas.

The Document API and the Search API have a lot of details and possibilities, this are just the basics. Take a look in the ES documentation to learn more about these two.

Before diving into details about indexing a document. Let's discuss about how ES executes the query.

.. image:: http://www.lucassaldanha.com/content/images/2015/11/query_api_in_action-2.png

When you send a query to your ES cluster, the query will hit one of the nodes (in a round robin basis) and then it figures out which shards should be queried.
Then the "query coordinator" node sends the query to each one of those shards to execute the query in parallel.
After each node answers the query with the partial result, the node merges the result and sends it back to the user.
One of the things that makes ES very fast in querying documents is that by default it already executes queries in parallel (traversing the shards).

Indexing
--------

There are other details that allows ES to be very effective in searching the documents.
When storing a document in ES it creates some internal data structures that makes the query perform better.
I'll talk about some basic details of ES indexing techniques.

Every document that is sent to ES to be stored go through a hashing algorithm and then is sent to a shard.
ES tries to distribute the document evenly across the shards of the index.

When storing the document ES creates an inverted index mapping the terms of the terms present on the document to the document itself.

.. image:: http://www.lucassaldanha.com/content/images/2015/11/inverted-index-example.png

Using the inverted index it's possible to search through the terms as a binary tree (using the alphabetical order) which reduces the time of the search.

Another important thing when storing documents is deciding the best way to store them enhancing the query speed.
When designing solutions using ElasticSearch the most important thing to think about when storing documents is: how would I like to query for this document?
This "query first" approach facilitates the use of all ES indexing possibilities to make our queries incredible fast.
That's when the Analyzers of ES are very important.

References
----------
* https://dzone.com/articles/elasticsearch-101