Install Elasticsearch
=====================

Docker compose file
-------------------
Create ``docker-compose.yml`` file include :

* 1 cluster with 4 nodes
* Kibana

::

    version: '2'

    services:
      gateway:
        image: beesightsoft/openjdk-elasticsearch:5.1.1
        environment:
          - UNICAST_HOSTS=master
          - TYPE=GATEWAY
        ports:
          - "9200:9200"
        volumes:
          - '$PWD/conf:/conf'

      master:
        image: beesightsoft/openjdk-elasticsearch:5.1.1
        environment:
          - UNICAST_HOSTS=gateway
          - TYPE=MASTER
        volumes:
          - '$PWD/conf:/conf'

      data:
        image: beesightsoft/openjdk-elasticsearch:5.1.1
        environment:
          - UNICAST_HOSTS=master,gateway
          - TYPE=DATA
        volumes:
          - '$PWD/data:/data'
          - '$PWD/conf:/conf'

      data1:
        image: beesightsoft/openjdk-elasticsearch:5.1.1
        environment:
          - UNICAST_HOSTS=master,gateway
          - TYPE=DATA
        volumes:
          - '$PWD/data1:/data'
          - '$PWD/conf:/conf'


      kibana:
        image: kibana:5.1.1
        ports:
          - '5601:5601'
        links:
          - gateway:elasticsearch

Run docker compose
------------------
::

    docker-composer up -d


Elasticsearch Cluster
---------------------

From any of your Elasticsearch servers, open this link to print the state of the cluster:
::

    http://$ip:9200/_cluster/state?pretty


Kibana
------

Kibana lets you visualize your Elasticsearch data and navigate the Elastic Stack
::

    http://$ip:5601

