Secure APIs
===========

Protecting Routes
-----------------
Use middleware  ``passport:api`` to protect function in controller

Example
::

    public function __construct()
    {
        $this->middleware('passport:api')->only(['getCurrentUser', 'changePassword']);
    }


Client Side
-----------
When using this method of authentication, client should send these header:

- ``X-Requested-With``: XMLHttpRequest
- ``Content-Type``: application/json
- ``Authorization``: Bearer xxx

Example
::

    $.ajaxSetup({
        headers: {
            'X-Requested-With': 'XMLHttpRequest',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer xxx'
        }
    });


