Login
=====

Credential username/email/password
----------------------------------
Returns
::

    League\OAuth2\Server\ResponseTypes\ResponseTypeInterface

Throws
::

     League\OAuth2\Server\Exception\OAuthServerException | Core\Authentication\Services\Exception\AdapterException

Arguments
::

    Core\Authentication\Services\Credentials\LocalCredentials $localCredential

Example
::

    $token = $this->adapter->login($localCredential)


Social network
--------------
Returns
::

    League\OAuth2\Server\ResponseTypes\ResponseTypeInterface

Throws
::

     League\OAuth2\Server\Exception\OAuthServerException | Core\Authentication\Services\Exception\AdapterException

Arguments
::

    Core\Authentication\Services\Credentials\SocialCredentials $socialCredential

Example
::

    $token = $this->adapter->login($socialCredential)


Sms/phone call Two Factor Authentication
----------------------------------------
- Send ``SMS`` one time token to phone number

**Via authy (twilio)**

Returns
::

   bool

Throws
::

     League\OAuth2\Server\Exception\OAuthServerException | Core\Authentication\Services\Exception\AdapterException

Arguments
::

    Core\Authentication\Services\Credentials\PhoneCredentials $phoneCredential

Example
::

    $this->adapter->enable2FA()->authyAdapter2FA()->send2FAViaSms()->login($phoneCredential);


**Via nexmo**

Returns
::

   bool

Throws
::

     League\OAuth2\Server\Exception\OAuthServerException | Core\Authentication\Services\Exception\AdapterException

Arguments
::

    Core\Authentication\Services\Credentials\PhoneCredentials $phoneCredential

Example
::

    $this->adapter->enable2FA()->nexmoAdapter2FA()->send2FAViaSms()->login($phoneCredential);

- Send ``Phone Call`` one time token to phone number

**Via authy (twilio)**

Returns
::

   bool

Throws
::

     League\OAuth2\Server\Exception\OAuthServerException | Core\Authentication\Services\Exception\AdapterException

Arguments
::

    Core\Authentication\Services\Credentials\PhoneCredentials $phoneCredential

Example
::

    $this->adapter->enable2FA()->authyAdapter2FA()->send2FAViaPhone()->login($phoneCredential);


**Via nexmo**

Returns
::

   bool

Throws
::

     League\OAuth2\Server\Exception\OAuthServerException | Core\Authentication\Services\Exception\AdapterException

Arguments
::

    Core\Authentication\Services\Credentials\PhoneCredentials $phoneCredential

Example
::

    $this->adapter->enable2FA()->nexmoAdapter2FA()->send2FAViaPhone()->login($phoneCredential);


- ``Verify`` sms/phone call one time token

**via authy (twilio)**

Returns
::

    bool|League\OAuth2\Server\ResponseTypes\ResponseTypeInterface

Throws
::

     League\OAuth2\Server\Exception\OAuthServerException | Core\Authentication\Services\Exception\AdapterException

Arguments
::

    Core\Authentication\Services\Credentials\PhoneCredentials $phoneCredential
    String $oneTimeToken

Example
::

    $token = $this->adapter->enable2FA()->authyAdapter2FA()->verify2FAToken($phoneCredential, $oneTimeToken);

**Via nexmo**

Returns
::

    bool|League\OAuth2\Server\ResponseTypes\ResponseTypeInterface

Throws
::

     League\OAuth2\Server\Exception\OAuthServerException | Core\Authentication\Services\Exception\AdapterException

Arguments
::

    Core\Authentication\Services\Credentials\PhoneCredentials $phoneCredential
    String $oneTimeToken

Example
::

    $token = $this->adapter->enable2FA()->nexmoAdapter2FA()->verify2FAToken($phoneCredential, $oneTimeToken);


Enable Two Factor Authentication
--------------------------------
- Install 2FA : :doc:`../2FA/install`
- Configure User model : :doc:`../2FA/configure-user-model`
- Register user :  :doc:`../2FA/register-user-two-factor-authentication`
- UnRegister user :  :doc:`../2FA/unregister-user-two-factor-authentication`


Email Two Factor Authentication
-------------------------------
- User model should extends ``Core\Push\Services\Eloquents\User``
- User model should implements ``TwoFactorAuthenticatableContract``
- Send ``Email`` one time token to user
Returns
::

   bool

Throws
::

     League\OAuth2\Server\Exception\OAuthServerException | Core\Authentication\Services\Exception\AdapterException

Arguments
::

    Core\Authentication\Services\Credentials\PhoneCredentials $localCredentials

Example
::

    $this->adapter->enable2FA()->emailAdapter2FA()->send2FAViaEmail()->login($localCredentials);

- Verify ``Email`` one time token

Returns
::

    bool|League\OAuth2\Server\ResponseTypes\ResponseTypeInterface

Throws
::

     League\OAuth2\Server\Exception\OAuthServerException | Core\Authentication\Services\Exception\AdapterException

Arguments
::

    Core\Authentication\Services\Credentials\LocalCredentials $localCredentials
    String $oneTimeToken

Example
::

    $token = $this->adapter->enable2FA()->emailAdapter2FA()->verify2FAToken($localCredentials, $oneTimeToken);
