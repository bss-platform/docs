Register
========

Returns
::

    Illuminate\Contracts\Auth\Authenticatable | Core\Authentication\Services\Exception\DuplicateUserException


Arguments
::

    Core\Authentication\Services\Contracts\Credentials $credential

Example
::

    $credentials = new LocalCredentials([
        'email' => array_get($parsedBody, 'email'),
        'username' => array_get($parsedBody, 'username'),
        'password' => array_get($parsedBody, 'password'),

        'client_id' => array_get($parseBody, 'client_id'),
        'client_secret' => array_get($parseBody, 'client_secret')
    ]);
    $credentials->setOptions(array_get($parsedBody, 'options', [])); //other options to save into DB
    $user = $this->adapter->register($credentials);


