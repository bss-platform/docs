Reset password
==============

Request reset password
----------------------
Returns
::

    bool

Throws
::

     League\OAuth2\Server\Exception\OAuthServerException

Arguments
::

    Core\Authentication\Services\Credentials\LocalCredentials $credential
    String $callbackLink

Example
::

    $credentials = new LocalCredentials([
        'email' => array_get($parsedBody, 'email')
    ]);
    $callbackLink = array_get($parsedBody, 'callbackLink', null);

    $success = $this->adapter->requestResetPassword($credentials, $callbackLink);


Process reset password
----------------------
Returns
::

    bool

Throws
::

     League\OAuth2\Server\Exception\OAuthServerException
     Core\Authentication\Services\Exception\TokenException
     Core\Authentication\Services\Exception\ValidationException

Arguments
::

    String $id
    String $code
    Array  $data
            password
            password_confirmation

Example
:::

    /**
    *  Process Request Reset password.
    *
    * @return bool
    * @throws OAuthServerException
    * @throws TokenException
    * @throws ValidationException
    * */


    $id = array_get($parsedBody, 'id');
    $code = array_get($parsedBody, 'code');
    $data = [
        'password' => array_get($parsedBody, 'password'),
        'password_confirmation' => array_get($parsedBody, 'password_confirmation')
    ];

    $success = $this->adapter->processResetPassword($id, $code, $data);
