Install
=======

Update composer.json
--------------------
::

    'repositories': [
        {
            'url': 'git@bitbucket.org:bss-vendor/authentication.git',
            'type': 'git'
        }
    ],
    'require': {
        'core/authentication': 'dev-master'
    }


Update composer
---------------
::

    composer update

Publish vendor
--------------
::

    php artisan vendor:publish --provider='Core\Authentication\Providers\AuthenticationServiceProvider' --tag='config'


==> Copied File [/extensions/core/authentication/src/config/config.php] To [/config/core.authentication.php]
::

    php artisan vendor:publish --provider="Core\Twofactor\Providers\TwofactorServiceProvider" --tag="config"

==> Copied File [/extensions/core/twofactor/src/config/config.php] To [/config/core.twofactor.php]


**Need to update the model user for two factor authentication in /config/core.twofactor.php**

::

    'tokensExpireIn' => env('CORE_AUTHENTICATION_TOKENS_EXPIRE_IN'),  //unit minutes
    'refreshTokensExpireIn' => env('CORE_AUTHENTICATION_REFRESH_TOKENS_EXPIRE_IN'), //unit minutes
    /*
     |--------------------------------------------------------------------------
     | Users
     |--------------------------------------------------------------------------
     |
     | Please provide the user model used authentication.
     |
     */

    'users' => [
        'model' => 'Core\Authentication\Services\Eloquents\AuthenticationUser',
    ],

    /*
    *  Multiple login at the same time
    *
    * */
    'multipleLogin' => false,


    /*
     * Reminders template
     *
     *
     */
    'reminder' => [
        'password_reminder_success' => 'core/authentication::auth/password_reminder_success',
        'password_reminder' => 'core/authentication::auth/password_reminder',
        'password_reminder_confirm' => 'core/authentication::auth/password_reminder_confirm',
        'user_password_reminder' => 'core/authentication::emails/user_password_reminder'
    ]

Environment
-----------
::

    CORE_AUTHENTICATION_TOKENS_EXPIRE_IN : minutes
    CORE_AUTHENTICATION_REFRESH_TOKENS_EXPIRE_IN  : minutes



Social Environment
------------------
Facebook, Google, ...

``config/cartalyst.sentinel-addons.social.php``
