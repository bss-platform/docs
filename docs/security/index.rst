Authenticate & Authorized
=========================
.. image:: ../images/coreauthentication.png
.. toctree::
    :maxdepth: 4
    :glob:

    install
    adapter
    create-app
    credential
    register
    login
    logout
    oauth2-token
    get-current-user
    change-password
    reset-password
    refresh-token
    secure-apis

