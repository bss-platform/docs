Credential
==========
Interface : ``Core\Authentication\Services\Contracts\Credentials``

username/password Credential
----------------------------
Returns
::

    Core\Authentication\Services\Credentials\LocalCredentials

Arguments
::

    grant_type - password
    client_id
    client_secret
    username - Username/Email
    password

Example
::

    $this->adapter = CoreAuthentication::local();
    $credentials = new LocalCredentials([
        'grant_type' => 'password',
        'client_id' => array_get($parsedBody, 'client_id'),
        'client_secret' => array_get($parsedBody, 'client_secret'),

        'username' => array_get($parsedBody, 'username'),
        'password' => array_get($parsedBody, 'password'),

    ]);

Facebook Credential
-------------------
Returns
::

    Core\Authentication\Services\Credentials\SocialCredentials

Arguments
::

    grant_type - social
    driver - facebook
    client_id
    client_secret
    access_token - Facebook access token

Example
::

    $this->adapter = CoreAuthentication::social();
    $credentials = new SocialCredentials([
        'grant_type' => 'social,
        'driver' => 'facebook',
        'client_id' => array_get($parseBody, 'client_id'),
        'client_secret' => array_get($parseBody, 'client_secret'),

        'access_token' => array_get($parseBody, 'access_token')
    ]);

Google Credential
-----------------
Returns
::

    Core\Authentication\Services\Credentials\SocialCredentials

Arguments
::

    grant_type - social
    driver - google
    client_id
    client_secret
    access_token - Google access token

Example
::

    $this->adapter = CoreAuthentication::social();
    $credentials = new SocialCredentials([
        'grant_type' => 'social,
        'driver' => 'google',
        'client_id' => array_get($parseBody, 'client_id'),
        'client_secret' => array_get($parseBody, 'client_secret'),

        'access_token' => array_get($parseBody, 'access_token')
    ]);

Linkedin Credential
-------------------
Returns
::

    Core\Authentication\Services\Credentials\SocialCredentials

Arguments
::

    grant_type - social
    driver - linkedin
    client_id
    client_secret
    access_token - Linkedin access token

Example
::

    $this->adapter = CoreAuthentication::social();
    $credentials = new SocialCredentials([
        'grant_type' => 'social,
        'driver' => 'linkedin',
        'client_id' => array_get($parseBody, 'client_id'),
        'client_secret' => array_get($parseBody, 'client_secret'),

        'access_token' => array_get($parseBody, 'access_token')
    ]);

Twitter Credential
------------------
Returns
::

    Core\Authentication\Services\Credentials\SocialCredentials

Arguments
::

    grant_type - social
    driver - twitter
    client_id
    client_secret
    oauth_token - Twitter oauth token
    oauth_token_secret - Twitter oauth token secret

Example
::

    $this->adapter = CoreAuthentication::social();
    $credentials = new SocialCredentials([
        'grant_type' => 'social,
        'driver' => 'twitter',
        'client_id' => array_get($parseBody, 'client_id'),
        'client_secret' => array_get($parseBody, 'client_secret'),

        //oauth 1 (twitter)
        'oauth_token' => array_get($parsedBody, 'oauth_token'),
        'oauth_token_secret' => array_get($parsedBody, 'oauth_token_secret')
    ]);


SMS Credential
--------------
Returns
::

    Core\Authentication\Services\Credentials\PhoneCredentials

Arguments
::

    grant_type - phone
    client_id
    client_secret
    phone_country_code
    phone_number

Example
::

    $this->adapter = CoreAuthentication::phone();
    $credentials = new PhoneCredentials([
        'grant_type' => 'phone',
        'client_id' => array_get($parsedBody, 'client_id'),
        'client_secret' => array_get($parsedBody, 'client_secret'),

        'phone_country_code' => array_get($parsedBody, 'phone_country_code'),
        'phone_number' => array_get($parsedBody, 'phone_number')
    ]);



Phone call Credential
---------------------
Returns
::

    Core\Authentication\Services\Credentials\PhoneCredentials

Arguments
::

    grant_type - phone
    client_id
    client_secret
    phone_country_code
    phone_number

Example
::

    $this->adapter = CoreAuthentication::phone();
    $credentials = new PhoneCredentials([
        'grant_type' => 'phone',
        'client_id' => array_get($parsedBody, 'client_id'),
        'client_secret' => array_get($parsedBody, 'client_secret'),

        'phone_country_code' => array_get($parsedBody, 'phone_country_code'),
        'phone_number' => array_get($parsedBody, 'phone_number')
    ]);


RefreshToken Credential
-----------------------
Returns
::

    Core\Authentication\Services\Credentials\RefreshTokenCredentials

Arguments
::

    client_id
    client_secret
    refresh_token

Example
::

    $credentials =  new RefreshTokenCredentials([
        'client_id' => array_get($parsedBody, 'client_id'),
        'client_secret' => array_get($parsedBody, 'client_secret'),
        'refresh_token' => array_get($parsedBody, 'refresh_token'),
    ]);


Common
------
Save other options into DB
::

    $credentials->setOptions(array_get($parsedBody, 'options', []));

