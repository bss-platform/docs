Logout
======
Returns
::
    void

Example
::

    $this->adapter->logout();
