Refresh Token
=============
Returns
::

    bool|ResponseTypeInterface

Throws
::

     League\OAuth2\Server\Exception\OAuthServerException

Arguments
::

    Core\Authentication\Services\Credentials\RefreshTokenCredentials $refreshTokenCredential

Example
::

    $refreshTokenCredential = new RefreshTokenCredentials([
        'client_id' => array_get($parsedBody, 'client_id'),
        'client_secret' => array_get($parsedBody, 'client_secret'),
        'refresh_token' => array_get($parsedBody, 'refresh_token'),
    ]);
    $token = $this->adapter->refreshToken($refreshTokenCredential);


