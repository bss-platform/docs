Change password
===============
Returns
::

    bool|ResponseTypeInterface

Throws
::

     League\OAuth2\Server\Exception\OAuthServerException

Arguments
::

    Core\Authentication\Services\Credentials\LocalCredentials $credential
    String $newPassword

Example
::

    $user = $this->adapter->getCurrentUser();
    $credentials = new LocalCredentials([
        'email' => $user->email,
        'password' => array_get($parsedBody, 'password')
    ]);

    $this->adapter->changePassword($credentials, array_get($parsedBody, 'newPassword'));

