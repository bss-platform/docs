Create App
----------
- Generate Encryption keys
::

    php artisan passport:keys
    Encryption keys generated successfully.

- Generate Password grant client for App name - bsscore
::

     php artisan passport:client --password
     What should we name the password grant client? [Laravel Password Grant Client]
     > bsscore
    Password grant client created successfully.
    Client ID: 1
    Client Secret: ugHcxD2kfkJxSaLVo4h0kbS1f0y1AEwWoIg7xq5r

- Use Password grant client - Client ID & Client Secret for requesting APIs
- Can enable/disable app
- Can create multiple app