Get current user
================
Returns
::

    Illuminate\Contracts\Auth\Authenticatable


Arguments
::


Example
::

    $user = $this->adapter->getCurrentUser();

