Adapter
=======

Local
-----
Returns
::

   Core\Authentication\Services\Adapters\Local


Arguments
::


Example
::

    CoreAuthentication::local();


Social
------
Returns
::

   Core\Authentication\Services\Adapters\Social


Arguments
::


Example
::

    CoreAuthentication::social();


Phone
-----
Returns
::

   Core\Authentication\Services\Adapters\Phone


Arguments
::


Example
::

    CoreAuthentication::phone();
