Install
=======

Update composer.json
--------------------
::

    "repositories": [
        {
            "url": "git@bitbucket.org:bss-vendor/vendor-impersonate.git",
            "type": "git"
        }
    ],
    "require": {
        "core-vendor/impersonate": "dev-master"
    }


Update composer
---------------
::

    composer update

Publish vendor
--------------
::

    php artisan vendor:publish --provider="CoreVendor\Impersonate\ImpersonateServiceProvider" --tag="config"


==> Copied File [/vendor/core-vendor/impersonate/config/config.php] To [/config/core.impersonate.php]

Migrate DB
----------
::

    php artisan migrate

    Migrating: 2017_06_15_000000_add_column_impersonate_to_users_table
    Migrated:  2017_06_15_000000_add_column_impersonate_to_users_table
