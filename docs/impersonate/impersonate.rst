Update User model
=================
::

    use \CoreVendor\Impersonate\Contracts\Impersonate;

Impersonate
===========
::

    $impersonator = $this->eloquentUser->find($fromID);
    $impersonated = $this->eloquentUser->find($toID);
    if ($impersonator->impersonate($impersonated)) {
        //ok
    } else {
         //failed
    }


Leave Impersonate
=================
::

    $impersonator = $this->eloquentUser->find($fromID);
    if ($impersonator->leaveImpersonation()) {
       //ok
    } else {
       //failed
    }
