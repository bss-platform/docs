Friendly ID
===========
Returns
::

   Friendly ID

Arguments
::

    Number $length - 0 : random

Example
::

    $friendlyIdFactory = \CoreVendor\Id\Generator\FriendlyGeneratorFactory::getGenerator();
    $id1 = $friendlyIdFactory->generate(0);
    $id2 = $friendlyIdFactory->generate(5);
    $id = $id1 . "_" . $id2; //combine friendly ids

