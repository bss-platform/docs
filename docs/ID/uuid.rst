UUID
====
Returns
::

   UUID object

Arguments
::

   version

Example
::

    $uuid = Uuid::uuid4()->toString();

