Usage
=====

Update method generate ID
-------------------------

``MyOverrides\Traits\UuidForKey``
::

    <?php

    namespace MyOverrides\Traits;

    use Ramsey\Uuid\Uuid;

    trait UuidForKey
    {
        /**
         * Boot the Uuid trait for the model.
         *
         * @return void
         */

        public static function bootUuidForKey()
        {
            static::creating(function ($model) {
                $model->{$model->getKeyName()} = Uuid::uuid4()->toString();
            });
        }
    }


Update Eloquent model
---------------------
::

    class MyEloquentModel
    {
        use \MyOverrides\Traits\UuidForKey;
        public $incrementing = false;

        ...
    }


