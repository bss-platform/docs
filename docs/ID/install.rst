Install
=======

Requirement
-----------
- Friendly ID : GMP - http://www.php.net/manual/en/gmp.installation.php
- UUID : https://github.com/ramsey/uuid

Update composer.json
--------------------
::

    "repositories": [
        {
            "url": "git@bitbucket.org:bss-vendor/vendor-id.git",
            "type": "git"
        }
    ],
    "require": {
        "core-vendor/id": "dev-master"
    }


Update composer
---------------
::

    composer update
