Configure user model
====================
- User model shoud extend ``Core\Push\Services\Eloquents\User``
- User model shoud implements ``Core\Twofactor\Services\Contracts\Auth\TwoFactor\Authenticatable``
- Update user model
Example
::

    use Core\Twofactor\Services\Auth\TwoFactor\Authenticatable as TwoFactorAuthenticatable;
    use Core\Twofactor\Services\Contracts\Auth\TwoFactor\Authenticatable as TwoFactorAuthenticatableContract;

    class TwoFactorUser extends \Core\Push\Services\Eloquents\User implements TwoFactorAuthenticatableContract
    {

        use TwoFactorAuthenticatable;

        protected $hidden = [
            'two_factor_options'
        ];

        public function createTwoFactorEmail()
        {
            $adapter = 'gmail';
            $title = ucfirst($adapter) . ' : Welcome to BSSCORE 2FA @2017';
            $dataView = [
                'url' => 'https://beesightsoft.com',
                'title' => $title,
                'background' => 'https://jakegroup.com/media/blog_main_2factor-authentication3.jpg',
                'content' => '
    Dear User,

    You are receiving this email because you requested a one time code for 2FA

    Enter this code to complete your login

    This token is below :'
            ];
            $mailable = new TwofactorEmail($dataView);
            return $mailable;
        }


        /**
         * Route notifications for the mail channel.
         *
         * @return string
         */
        public function routeNotificationForMail()
        {
            return $this->email;
        }
    }

- Create Two Factor Email
Example
::

    use Core\Twofactor\Services\Mail\TwofactorEmail as CoreTwofactorEmail;

    class TwofactorEmail extends CoreTwofactorEmail
    {

        /**
         * Create a new job instance.
         *
         * @var  array $options
         *
         * @return void
         */

        public function __construct($options)
        {
            $this->options = $options;
        }

        /**
         * Build the message.
         *
         * @return $this
         */
        public function build()
        {
            return $this
                ->from(env('MAIL_USERNAME'))
                ->view('core/demo::email.twofactor')
                ->with($this->getData());
        }
    }



