Two Factor Authentication
=========================
.. image:: ../images/coretwofactor.png
.. toctree::
    :maxdepth: 4
    :glob:

    install
    adapter
    create-your-own-adapter
    configure-user-model
    register-user-two-factor-authentication
    unregister-user-two-factor-authentication
    send-one-time-token
    verify-one-time-token

