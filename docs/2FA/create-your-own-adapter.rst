Create your own Adapter
=======================

Defined Namespace
-----------------
::

    namespace Core\Twofactor\Services\Adapters;

Defined Interface
-----------------
::

    use Core\Twofactor\Services\Contracts\Auth\TwoFactor\PhoneToken as SendPhoneTokenContract;
    use Core\Twofactor\Services\Contracts\Auth\TwoFactor\SMSToken as SendSMSTokenContract;
    use Core\Twofactor\Services\Contracts\Auth\TwoFactor\Provider as BaseProvider;

- ``SendPhoneTokenContract`` Interface
::

    <?php

    namespace Core\Twofactor\Services\Contracts\Auth\TwoFactor;

    use Core\Twofactor\Services\Contracts\Auth\TwoFactor\Authenticatable as TwoFactorAuthenticatable;

    interface PhoneToken
    {
        /**
         * Start the user two-factor authentication via phone call.
         *
         * @param \Core\Twofactor\Services\Contracts\Auth\TwoFactor\Authenticatable $user
         *
         * @return void
         */
        public function sendPhoneCallToken(TwoFactorAuthenticatable $user);
    }

- ``SendSMSTokenContract`` Interface
::

    <?php

    namespace Core\Twofactor\Services\Contracts\Auth\TwoFactor;

    use Core\Twofactor\Services\Contracts\Auth\TwoFactor\Authenticatable as TwoFactorAuthenticatable;

    interface PhoneToken
    {
        /**
         * Start the user two-factor authentication via phone call.
         *
         * @param \Core\Twofactor\Services\Contracts\Auth\TwoFactor\Authenticatable $user
         *
         * @return void
         */
        public function sendPhoneCallToken(TwoFactorAuthenticatable $user);
    }

- ``BaseProvider`` Interface
::

    <?php

    namespace Core\Twofactor\Services\Contracts\Auth\TwoFactor;

    use Core\Twofactor\Services\Contracts\Auth\TwoFactor\Authenticatable as TwoFactorAuthenticatable;

    interface Provider
    {
        /**
         * Determine if the given user has two-factor authentication enabled.
         *
         * @param \Core\Twofactor\Services\Contracts\Auth\TwoFactor\Authenticatable $user
         *
         * @return bool
         */
        public function isEnabled(TwoFactorAuthenticatable $user);

        /**
         * Register the given user with the provider.
         *
         * @param \Core\Twofactor\Services\Contracts\Auth\TwoFactor\Authenticatable $user
         * @param bool                                                     $sms
         *
         * @return bool
         * @throws
         */
        public function register(TwoFactorAuthenticatable $user, $sms = false);

        /**
         * Determine if the given token is valid for the given user.
         *
         * @param \Core\Twofactor\Services\Contracts\Auth\TwoFactor\Authenticatable $user
         * @param string                                                   $token
         *
         * @return bool
         * @throws
         */
        public function tokenIsValid(TwoFactorAuthenticatable $user, $token);

        /**
         * Delete the given user from the provider.
         *
         * @param \Core\Twofactor\Services\Contracts\Auth\TwoFactor\Authenticatable $user
         *
         * @return bool
         * @throws
         */
        public function delete(TwoFactorAuthenticatable $user);
    }


Write your own adapter
----------------------
Example
::

    class Nexmo implements BaseProvider, SendSMSTokenContract, SendPhoneTokenContract
    {

        const VERSION = '1.0.0';

        /**
         * Array containing configuration data.
         *
         * @var array
         */
        private $config;

        /*
         * CoreCurl $headers
         *
         * */
        private $adapter;


        /**
         * Authy constructor.
         */
        public function __construct()
        {
            $configPushDriver = config('core.twofactor.nexmo');
            $config = array_get($configPushDriver, config('core.push.environment'));
            $this->config = $config;

            $this->adapter = new NexmoService(new NexmoCredentials($this->config['key'], $this->config['secret']));

        }

        private function __getUserAgent()
        {
            return sprintf(
                'Nexmo/%s (%s-%s-%s; PHP %s)',
                self::VERSION,
                php_uname('s'),
                php_uname('r'),
                php_uname('m'),
                phpversion()
            );
        }

        /**
         * Determine if the given user has two-factor authentication enabled.
         *
         * @param \Core\Twofactor\Services\Contracts\Auth\TwoFactor\Authenticatable $user
         *
         * @return bool
         */
        public function isEnabled(TwoFactorAuthenticatable $user)
        {
            return isset($user->getTwoFactorAuthProviderOptions()['id']);
        }

        /**
         * Register the given user with the provider.
         *
         * @param \Core\Twofactor\Services\Contracts\Auth\TwoFactor\Authenticatable $user
         * @param bool $sms
         *
         * @return bool
         * @throws AdapterException
         */
        public function register(TwoFactorAuthenticatable $user, $sms = false)
        {
            try {
                $options = [
                    'adapter' => 'nexmo',
                    'sms' => $sms,
                ];
                $user->setTwoFactorAuthProviderOptions($options);
                $user->save();
                return true;
            } catch (Exception $e) {
                throw  new AdapterException($e->getMessage());
            }


        }

        /**
         * Send the user two-factor authentication token via SMS.
         *
         * @param \Core\Twofactor\Services\Contracts\Auth\TwoFactor\Authenticatable $user
         *
         * @return bool
         * @throws AdapterException
         */
        public function sendSmsToken(TwoFactorAuthenticatable $user)
        {
            try {
                $number = $user->getAuthCountryCode() . $user->getAuthPhoneNumber();
                $brand = array_get($this->config, 'sms_from', 'bss.core');
                $additional = array_merge(
                    array_get($this->config, 'additional', []),
                    [
                        'require_type' => 'Mobile', //Mobile,Landline,All
                    ]
                );
                $verificationRequest = new Verification($number, $brand, $additional);
                $this->adapter->verify()->start($verificationRequest);


                $options = $user->getTwoFactorAuthProviderOptions();
                $options['id'] = $verificationRequest->getRequestId();

                $user->setTwoFactorAuthProviderOptions($options);
                $user->save();
                return true;
            } catch (\Exception $e) {
                throw  new AdapterException($e->getMessage());
            }
        }

        /**
         * Start the user two-factor authentication via phone call.
         *
         * @param \Core\Twofactor\Services\Contracts\Auth\TwoFactor\Authenticatable $user
         *
         * @return bool
         * @throws AdapterException
         */
        public function sendPhoneCallToken(TwoFactorAuthenticatable $user)
        {
            try {
                $number = $user->getAuthCountryCode() . $user->getAuthPhoneNumber();
                $brand = array_get($this->config, 'sms_from', 'bss.core');
                $additional = array_merge(
                    array_get($this->config, 'additional', []),
                    [
                        'require_type' => 'Landline', //Mobile,Landline,All
                    ]
                );

                $verificationRequest = new Verification($number, $brand, $additional);
                $this->adapter->verify()->start($verificationRequest);


                $options = $user->getTwoFactorAuthProviderOptions();
                $options['id'] = $verificationRequest->getRequestId();

                $user->setTwoFactorAuthProviderOptions($options);
                $user->save();
                return true;
            } catch (\Exception $e) {
                throw  new AdapterException($e->getMessage());
            }
        }

        /**
         * Determine if the given token is valid for the given user.
         *
         * @param \Core\Twofactor\Services\Contracts\Auth\TwoFactor\Authenticatable $user
         * @param string $token
         *
         * @return bool
         * @throws AdapterException
         */
        public function tokenIsValid(TwoFactorAuthenticatable $user, $token)
        {
            try {
                $options = $user->getTwoFactorAuthProviderOptions();
                $verification = $this->adapter->verify()->check(array_get($options, 'id', ''), $token);
                return $verification->getStatus() == "0";
            } catch (\Exception $e) {
                throw  new AdapterException($e->getMessage());
            }
        }

        /**
         * Delete the given user from the provider.
         *
         * @param \Core\Twofactor\Services\Contracts\Auth\TwoFactor\Authenticatable $user
         *
         * @return bool
         * @throws AdapterException
         */
        public function delete(TwoFactorAuthenticatable $user)
        {
            try {
                $user->setTwoFactorAuthProviderOptions([]);
                $user->save();
                return true;
            } catch (\Exception $e) {
                throw  new AdapterException($e->getMessage());
            }
        }

        /**
         * Determine if the given user should be sent two-factor authentication token via SMS/phone call.
         *
         * @param \Core\Twofactor\Services\Contracts\Auth\TwoFactor\Authenticatable $user
         *
         * @return bool
         */
        public function canSendToken(TwoFactorAuthenticatable $user)
        {
            $sendToken = collect(
                $user->getTwoFactorAuthProviderOptions()
            )->filter(function ($option, $key) {
                return ($option && in_array($key, ['sms', 'phone', 'email'])) ? $option : null;
            });

            return ($this->isEnabled($user) && (!$sendToken->isEmpty())) ? true : false;
        }
    }



Use your own adapter
--------------------
Returns
::

    Core\Twofactor\Services\Adapters\{ucFirst($adapterName)}

Arguments
::

     String $adapterName

Example
::

    $adapterName = 'Nexmo';
    $adapter = CoreTwoFactor::setProvider(lcfirst($adapterName));


