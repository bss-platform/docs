Adapter
=======

Authy
-----
Returns
::

   Core\Twofactor\Services\Adapters\Authy


Example
::

   $this->twoFactor = CoreTwoFactor::authy();


Nexmo
-----
Returns
::

   Core\Twofactor\Services\Adapters\Nexmo


Example
::

   $this->twoFactor = CoreTwoFactor::nexmo();

Email
-----
Returns
::

   Core\Twofactor\Services\Adapters\Email

Example
::

   $this->twoFactor = CoreTwoFactor::email();


References
----------
* [Authy](https://dashboard.authy.com)
* [Nexmo](https://dashboard.nexmo.com)
