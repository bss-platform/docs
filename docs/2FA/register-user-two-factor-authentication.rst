Register user two factor authentication
=======================================

Register Phone Number
---------------------
Returns
::

   bool
Throws
::

    Core\Twofactor\Services\Exception\AdapterException

Arguments
::

    Core\Twofactor\Services\Contracts\Auth\TwoFactor\Authenticatable $user

Example
::

    $phone = '0902466104';
    $code = 84;

    $user = $this->eloquentUser->find(1);
    $user->setAuthPhoneInformation(
        $code, $phone
    );

    $this->twoFactor->register($user);


Register Email
--------------
Returns
::

   bool

Throws
::

    Core\Twofactor\Services\Exception\AdapterException

Arguments
::

    Core\Twofactor\Services\Contracts\Auth\TwoFactor\Authenticatable $user

Example
::

    $user = $this->eloquentUser->find(1);
    $user->email = 'tai.phung@beesightsoft.com';
    $user->save();

    $this->twoFactor->register($user);