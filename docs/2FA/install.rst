Install
=======

Update composer.json
--------------------
::

    "repositories": [
        {
            "url": "git@bitbucket.org:bss-vendor/twofactor.git",
            "type": "git"
        }
    ],
    "require": {
        "core/twofactor": "dev-master"
    }


Update composer
---------------
::

    composer update

Publish vendor
--------------
::

    php artisan vendor:publish --provider="Core\Twofactor\Providers\TwofactorServiceProvider" --tag="config"


==> Copied File [/extensions/core/twofactor/src/config/config.php] To [/config/core.twofactor.php]


Environment
-----------
::

    #############CORE TWO FACTOR #############
    #config/core.push.php
    CORE_PUSH_ENVIRONMENT=development

    ##TWILIO##
    CORE_PUSH_TWILIO_ACCOUNT_SID=AC3c5569a53af83a14c128898a88de72de
    CORE_PUSH_TWILIO_AUTH_TOKEN=5a04665e4264368d6eba38f734449420
    CORE_PUSH_TWILIO_FROM=+17742216319
    ##TWILIO##

    ##PLIVO##
    CORE_PUSH_PLIVO_ACCOUNT_SID=AC3c5569a53af83a14c128898a88de72de
    CORE_PUSH_PLIVO_AUTH_TOKEN=5a04665e4264368d6eba38f734449420
    CORE_PUSH_PLIVO_FROM=+17742216319
    ##PLIVO##

    ##NEXMO##
    CORE_PUSH_NEXMO_KEY=9179556c
    CORE_PUSH_NEXMO_SECRET=693c229db3ccea1f
    CORE_PUSH_NEXMO_FROM=NEXMO
    ##NEXMO##

    ##AUTHY##
    CORE_PUSH_AUTHY_API_KEY=tc4tYAE5Cp7GeFqDQrrz8p64FV11q4J4
    ##AUTHY##

    #############CORE TWO FACTOR #############

Update providers
----------------
Register the Two Factor Authentication service provider by adding it to the providers array  in the ``app/config/app.php`` file.
::

    "providers" => [
        ...

        //Two Factor
        Core\Twofactor\Services\Providers\TwofactorServiceProvider::class,

        //Curl
        Ixudra\Curl\CurlServiceProvider::class,

