Send one time token
===================

SMS
---
Returns
::

   bool

Throws
::

    Core\Twofactor\Services\Exception\AdapterException

Arguments
::

    Core\Twofactor\Services\Contracts\Auth\TwoFactor\Authenticatable $user
::

    $this->twoFactor->sendSmsToken($user);


Phone Call
----------
Returns
::

   bool

Throws
::

    Core\Twofactor\Services\Exception\AdapterException

Arguments
::

    Core\Twofactor\Services\Contracts\Auth\TwoFactor\Authenticatable $user
::

    $this->twoFactor->sendPhoneCallToken($user);

Email
-----
Returns
::

   bool

Throws
::

    Core\Twofactor\Services\Exception\AdapterException

Arguments
::

    Core\Twofactor\Services\Contracts\Auth\TwoFactor\Authenticatable $user
::

    $title = 'Gmail : Welcome to BSSCORE 2FA @2017';
    $dataView = [
        'url' => 'https://beesightsoft.com',
        'title' => $title,
        'background' => 'https://jakegroup.com/media/blog_main_2factor-authentication3.jpg',
        'content' => '
            Dear User,

            You are receiving this email because you requested a one time code for 2FA

            Enter this code to complete your login

            This token is below :'
    ];
    $mailable = new TwofactorEmail($dataView);
    $this->twoFactor->sendEmailToken($user, $mailable);


Combine with authentication
---------------------------
Check the :doc:`security/login` page for more details.