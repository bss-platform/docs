Unregister user two factor authentication
=========================================
Returns
::

   bool

Throws
::

    Core\Twofactor\Services\Exception\AdapterException

Arguments
::

    Core\Twofactor\Services\Contracts\Auth\TwoFactor\Authenticatable $user

Example
::

    $this->twoFactor->delete($user);
