Preface
=======

BSS  Platform application provides a very flexible and extensible way of building your custom application.

It gives you a basic installation to get you quick-started with content management, themeable views, application extensions and much more.

Pretty much everything on Platform can be extended and overwritten so you can add your own functionality.

Features
--------
Platform is not just another CMS, it's a starting point for you to build your application providing the tools you need to get the job done as easy as possible.
::


    Authentication & Authorization
    Role based user management & permissions
    Social Authentication (OAuth, OAuth 2)
    Powerful Theme System
    Powerful Extension System
    Twitter Bootstrap 3.3 ready
    Pages
    Content
    Menus
    Settings
    Tags
    Attributes
    Scaffolding
    Widgets
    Media
    Laravel 5.4



Concepts
--------
The main goal of Platform is flexibility. Platform aims to be as unobtrusive to your application as possible, while providing all the features to make it awesome and save you time.
The app folder is almost identical to a stock-standard Laravel 5 app folder, with a few registered service providers and preset configurations.

The end result? You can continue to make any application you would like without having to conform to Platform "standards" or "practices".

Want to install Platform for an administration area but build a completely custom website? Sure, just start working with

    ``app/Http/routes.php``


as you normally would. Utilize our API, data and extensions where you need, they won't get in your way.

Extensibility
-------------
Platform 5 was made to be even more extendable than Platform 1. A number of key files to get you off the ground with extending Platform are


    * ``app/hooks.php``
    * ``app/functions.php``
    * ``app/overrides/models.php (1)``
    * ``app/overrides/services.php (1)``


(1) You can read more about overrides here.
These files provide a number of templates and boilerplate code for you to override extension classes, hook into system events and add custom logic.