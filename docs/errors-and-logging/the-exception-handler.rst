The Exception Handler
=====================


The Report Method
-----------------

All exceptions are handled by the ``App\Exceptions\Handler`` class.
This class contains two methods:  report and render.
We'll examine each of these methods in detail.
The report method is used to log exceptions or send them to an external service like Bugsnag or Sentry.
By default, the report method simply passes the exception to the base class where the exception is logged.
However, you are free to log exceptions however you wish.

For example, if you need to report different types of exceptions in different ways, you may use the PHP ``instanceof`` comparison operator:
::

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
         if ($exception instanceof \League\OAuth2\Server\Exception\OAuthServerException) {

            throw new \App\Exceptions\PassportHttpException($exception);

        } elseif ($exception instanceof \Cartalyst\Sentinel\Checkpoints\ThrottlingException) {

            throw new \App\Exceptions\ThrottleCheckpointHttpException($exception);

        } elseif ($exception instanceof \League\OAuth2\Client\Provider\Exception\IdentityProviderException) {

            throw new \App\Exceptions\IdentityOAuth2HttpException($exception);

        } elseif ($exception instanceof \League\OAuth2\Server\Exception\UniqueTokenIdentifierConstraintViolationException) {

            throw new \App\Exceptions\UniqueTokenIdentifierHttpException($exception);

        } else {

            return parent::report($exception);

        }
    }

Ignoring Exceptions By Type
---------------------------

The ``$dontReport`` property of the exception handler contains an array of exception types that will not be logged.
For example, exceptions resulting from 404 errors, as well as several other types of errors, are not written to your log files.
You may add other exception types to this array as needed:
::

    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        HttpException::class,
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,
        \League\OAuth2\Server\Exception\OAuthServerException::class,
        \App\Exceptions\PassportHttpException::class,
    ];

The Render Method
-----------------

The render method is responsible for converting a given exception into an HTTP response that should be sent back to the browser.
By default, the exception is passed to the base class which generates a response for you.
However, you are free to check the exception type or return your own custom response:
::

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if ($exception instanceof \App\Exceptions\PassportHttpException) {

            $errorMessage = $exception->getMessage();
            $errorCode = 'xxx';
            switch ($exception->getErrorType()) {
                case 'unsupported_grant_type':
                    break;
                case 'access_denied':
                    switch ($exception->getHint()) {
                        case 'Missing':
                            //Missing "Authorization" header
                            break;
                        case 'Access token could not be verified':
                            //Verify if the key matches with the one that created the signature
                            break;
                        case 'Access token is invalid':
                            //Expired date
                            break;
                        case 'Access token has been revoked':
                            //Access token has been revoked
                            break;
                        case 'The user denied the request':
                            //The user denied the request
                            break;
                        case 'The resource owner or authorization server denied the request.':
                            //The resource owner or authorization server denied the request.
                            break;
                        case 'Error while decoding to JSON':
                            //RuntimeException
                            break;
                        default:
                            //Other exception
                            break;


                    }
                    break;
                case 'invalid_client':
                    //Client authentication failed
                    break;
                case 'server_error':
                    //The authorization server encountered an unexpected condition which prevented it from fulfilling
                    break;
                case 'invalid_scope':
                    //The requested scope is invalid, unknown, or malformed
                    break;
                case 'invalid_request':
                    //The request is missing a required parameter, includes an invalid parameter value, ' .
                    //'includes a parameter more than once, or is otherwise malformed.
                    break;
                case 'invalid_credentials':
                    //The user credentials were incorrect.
                    break;
                case 'invalid_grant':
                    //The provided authorization grant (e.g., authorization code, resource owner credentials) or refresh token '
                    //. 'is invalid, expired, revoked, does not match the redirection URI used in the authorization request, '
                    //. 'or was issued to another client.
                    break;

                default:
                    break;
            }
            return response([
                'error' => true,
                'data' => null,
                'errors' => [[
                    'errorMessage' => $errorMessage,
                    'errorCode' => $errorCode
                ]]
            ]);
        } elseif ($exception instanceof \App\Exceptions\ThrottleCheckpointHttpException) {
            return response([
                'error' => true,
                'data' => null,
                'errors' => [[
                    'errorMessage' => $exception->getMessage(),
                    'errorCode' => 'xxx'
                ]]
            ], 400);
        } elseif ($exception instanceof \App\Exceptions\IdentityOAuth2HttpException) {
            return response([
                'error' => true,
                'data' => null,
                'errors' => [[
                    'errorMessage' => $exception->getMessage(),
                    'errorCode' => 'xxx'
                ]]
            ], 400);
        } elseif ($exception instanceof \App\Exceptions\UniqueTokenIdentifierHttpException) {
            return response([
                'error' => true,
                'data' => null,
                'errors' => [[
                    'errorMessage' => $exception->getMessage(),
                    'errorCode' => 'xxx'
                ]]
            ], 400);
        } else {
            return parent::render($request, $exception);
        }

    }
