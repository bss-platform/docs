Logging
=======

Laravel provides a simple abstraction layer on top of the powerful Monolog library.
By default, Laravel is configured to create a log file for your application in the ``storage/logs`` directory.
You may write information to the logs using the Log facade:
::

    <?php

    namespace App\Http\Controllers;

    use App\User;
    use Illuminate\Support\Facades\Log;
    use App\Http\Controllers\Controller;

    class UserController extends Controller
    {
        /**
         * Show the profile for the given user.
         *
         * @param  int  $id
         * @return Response
         */
        public function showProfile($id)
        {
            Log::info('Showing user profile for user: '.$id);

            return view('user.profile', ['user' => User::findOrFail($id)]);
        }
    }

The logger provides the eight logging levels defined in RFC 5424: emergency, alert, critical, error, warning, notice, info and debug.
::

    Log::emergency($message);
    Log::alert($message);
    Log::critical($message);
    Log::error($message);
    Log::warning($message);
    Log::notice($message);
    Log::info($message);
    Log::debug($message);
    Contextual Information

An array of contextual data may also be passed to the log methods. This contextual data will be formatted and displayed with the log message:
::

    Log::info('User failed to login.', ['id' => $user->id]);

Accessing The Underlying Monolog Instance

Monolog has a variety of additional handlers you may use for logging. If needed, you may access the underlying Monolog instance being used by Laravel:
::

    $monolog = Log::getMonolog();