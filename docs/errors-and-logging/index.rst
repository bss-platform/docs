Errors & Logging
================

When you start a new Laravel project, error and exception handling is already configured for you.
The ``App\Exceptions\Handler`` class is where all exceptions triggered by your application are logged and then rendered back to the user.
We'll dive deeper into this class throughout this documentation.

For logging, Laravel utilizes the ``Monolog`` library, which provides support for a variety of powerful log handlers.
Laravel configures several of these handlers for you, allowing you to choose between a single log file, rotating log files, or writing error information to the system log.


.. toctree::
    :maxdepth: 4
    :glob:

    configuration
    the-exception-handler
    http-exceptions
    logging