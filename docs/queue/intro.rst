Intro
=====

Laravel queues provide a unified API across a variety of different queue backends, such as ``Beanstalk``, ``Amazon SQS``, ``Redis``, or even a relational database.
Queues allow you to defer the processing of a time consuming task, such as sending an email, until a later time.
Deferring these time consuming tasks drastically speeds up web requests to your application.


The queue configuration file is stored in ``config/queue.php``.
In this file you will find connection configurations for each of the queue drivers that are included with the framework, which includes a database, Beanstalkd, Amazon SQS, Redis, and a synchronous driver that will execute jobs immediately (for local use).
A null queue driver is also included which simply discards queued jobs.



References
----------
* https://laravel.com/docs/5.3/queues