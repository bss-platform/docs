Creating Jobs
=============


Generating Job Classes
----------------------

By default, all of the queueable jobs for your application are stored in the ``app/Jobs`` directory.
If the  ``app/Jobs`` directory doesn't exist, it will be created when you run the ``make:job`` Artisan command.
You may generate a new queued job using the Artisan CLI:
::

    php artisan make:job SendReminderEmail

The generated class will implement the ``Illuminate\Contracts\Queue\ShouldQueue `` interface, indicating to Laravel that the job should be pushed onto the queue to run asynchronously.


Class Structure
---------------

Job classes are very simple, normally containing only a handle method which is called when the job is processed by the queue.
To get started, let's take a look at an example job class.
In this example, we'll pretend we manage a podcast publishing service and need to process the uploaded podcast files before they are published:
::

    <?php

    namespace App\Jobs;

    use App\Podcast;
    use App\AudioProcessor;
    use Illuminate\Bus\Queueable;
    use Illuminate\Queue\SerializesModels;
    use Illuminate\Queue\InteractsWithQueue;
    use Illuminate\Contracts\Queue\ShouldQueue;

    class ProcessPodcast implements ShouldQueue
    {
        use InteractsWithQueue, Queueable, SerializesModels;

        protected $podcast;

        /**
         * Create a new job instance.
         *
         * @param  Podcast  $podcast
         * @return void
         */
        public function __construct(Podcast $podcast)
        {
            $this->podcast = $podcast;
        }

        /**
         * Execute the job.
         *
         * @param  AudioProcessor  $processor
         * @return void
         */
        public function handle(AudioProcessor $processor)
        {
            // Process uploaded podcast...
        }
    }

In this example, note that we were able to pass an Eloquent model directly into the queued job's constructor.
Because of the ``SerializesModels`` trait that the job is using, Eloquent models will be gracefully serialized and unserialized when the job is processing.
If your queued job accepts an Eloquent model in its constructor, only the identifier for the model will be serialized onto the queue.
When the job is actually handled, the queue system will automatically re-retrieve the full model instance from the database.
It's all totally transparent to your application and prevents issues that can arise from serializing full Eloquent model instances.

The ``handle`` method is called when the job is processed by the queue. Note that we are able to type-hint dependencies on the ``handle`` method of the job.
The Laravel service container automatically injects these dependencies.

Binary data, such as raw image contents, should be passed through the base64_encode function before being passed to a queued job.
Otherwise, the job may not properly serialize to JSON when being placed on the queue.

