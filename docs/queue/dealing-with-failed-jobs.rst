Dealing With Failed Jobs
========================

Sometimes your queued jobs will fail.
Don't worry, things don't always go as planned! Laravel includes a convenient way to specify the maximum number of times a job should be attempted.
After a job has exceeded this amount of attempts, it will be inserted into the ``failed_jobs`` database table.
To create a migration for the ``failed_jobs`` table, you may use the queue:failed-table command:
::

    php artisan queue:failed-table

    php artisan migrate

Then, when running your queue worker, you should specify the maximum number of times a job should be attempted using the ``--tries`` switch on the ``queue:work`` command.
If you do not specify a value for the ``--tries`` option, jobs will be attempted indefinitely:
::

    php artisan queue:work redis --tries=3

Cleaning Up After Failed Jobs
-----------------------------

You may define a ``failed`` method directly on your job class, allowing you to perform job specific clean-up when a failure occurs.
This is the perfect location to send an alert to your users or revert any actions performed by the job.
The ``Exception`` that caused the job to fail will be passed to the  failed method:
::

    <?php

    namespace App\Jobs;

    use Exception;
    use App\Podcast;
    use App\AudioProcessor;
    use Illuminate\Bus\Queueable;
    use Illuminate\Queue\SerializesModels;
    use Illuminate\Queue\InteractsWithQueue;
    use Illuminate\Contracts\Queue\ShouldQueue;

    class ProcessPodcast implements ShouldQueue
    {
        use InteractsWithQueue, Queueable, SerializesModels;

        protected $podcast;

        /**
         * Create a new job instance.
         *
         * @param  Podcast  $podcast
         * @return void
         */
        public function __construct(Podcast $podcast)
        {
            $this->podcast = $podcast;
        }

        /**
         * Execute the job.
         *
         * @param  AudioProcessor  $processor
         * @return void
         */
        public function handle(AudioProcessor $processor)
        {
            // Process uploaded podcast...
        }

        /**
         * The job failed to process.
         *
         * @param  Exception  $exception
         * @return void
         */
        public function failed(Exception $exception)
        {
            // Send user notification of failure, etc...
        }
    }

Failed Job Events
-----------------

If you would like to register an event that will be called when a job fails, you may use the  ``Queue::failing`` method.
This event is a great opportunity to notify your team via email or HipChat.
For example, we may attach a callback to this event from the ``AppServiceProvider`` that is included with Laravel:
::

    <?php

    namespace App\Providers;

    use Illuminate\Support\Facades\Queue;
    use Illuminate\Queue\Events\JobFailed;
    use Illuminate\Support\ServiceProvider;

    class AppServiceProvider extends ServiceProvider
    {
        /**
         * Bootstrap any application services.
         *
         * @return void
         */
        public function boot()
        {
            Queue::failing(function (JobFailed $event) {
                // $event->connectionName
                // $event->job
                // $event->exception
            });
        }

        /**
         * Register the service provider.
         *
         * @return void
         */
        public function register()
        {
            //
        }
    }

Retrying Failed Jobs
--------------------

To view all of your failed jobs that have been inserted into your ``failed_jobs`` database table, you may use the ``queue:failed`` Artisan command:
::

    php artisan queue:failed


The ``queue:failed`` command will list the job ID, connection, queue, and failure time.
The job ID may be used to retry the failed job. For instance, to retry a failed job that has an ID of 5, issue the following command:
::

    php artisan queue:retry 5

To retry all of your failed jobs, execute the ``queue:retry`` command and pass all as the ID:
::

    php artisan queue:retry all

If you would like to delete a failed job, you may use the ``queue:forget`` command:
::

    php artisan queue:forget 5

To delete all of your failed jobs, you may use the ``queue:flush`` command:
::

    php artisan queue:flush