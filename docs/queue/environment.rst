Environment
===========

You should configure a Redis database connection in your ``.env`` configuration file.
::

    QUEUE_DRIVER=file
    QUEUE_DRIVER=rabbitmq

    RABBITMQ_HOST=192.168.99.100
    RABBITMQ_PORT=5672
    RABBITMQ_VHOST=my_vhost
    RABBITMQ_LOGIN=user
    RABBITMQ_PASSWORD=password
    RABBITMQ_QUEUE=queue_name


    REDIS_HOST=127.0.0.1
    REDIS_PASSWORD=null
    REDIS_PORT=6379
