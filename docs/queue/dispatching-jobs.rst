Dispatching Jobs
================

Once you have written your job class, you may ``dispatch`` it using the dispatch helper.
The only argument you need to pass to the ``dispatch`` helper is an instance of the job:
::

    <?php

    namespace App\Http\Controllers;

    use App\Jobs\ProcessPodcast;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class PodcastController extends Controller
    {
        /**
         * Store a new podcast.
         *
         * @param  Request  $request
         * @return Response
         */
        public function store(Request $request)
        {
            // Create podcast...

            dispatch(new ProcessPodcast($podcast));
        }
    }

The ``dispatch`` helper provides the convenience of a short, globally available function, while also being extremely easy to test.
Check out the Laravel testing documentation to learn more.

Delayed Dispatching
-------------------

If you would like to delay the execution of a queued job, you may use the ``delay`` method on your job instance.
The ``delay`` method is provided by the ``Illuminate\Bus\Queueable`` trait, which is included by default on all generated job classes.
For example, let's specify that a job should not be available for processing until 10 minutes after it has been dispatched:
::

    <?php

    namespace App\Http\Controllers;

    use Carbon\Carbon;
    use App\Jobs\ProcessPodcast;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class PodcastController extends Controller
    {
        /**
         * Store a new podcast.
         *
         * @param  Request  $request
         * @return Response
         */
        public function store(Request $request)
        {
            // Create podcast...

            $job = (new ProcessPodcast($podcast))
                        ->delay(Carbon::now()->addMinutes(10));

            dispatch($job);
        }
    }

**The Amazon SQS queue service has a maximum delay time of 15 minutes.**

Customizing The Queue & Connection
----------------------------------

**Dispatching To A Particular Queue**

By pushing jobs to different queues, you may ``categorize`` your queued jobs and even prioritize how many workers you assign to various queues.
Keep in mind, this does not push jobs to different queue ``connections`` as defined by your queue configuration file, but only to specific queues within a single connection.
To specify the queue, use the ``onQueue`` method on the job instance:
::

    <?php

    namespace App\Http\Controllers;

    use App\Jobs\ProcessPodcast;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class PodcastController extends Controller
    {
        /**
         * Store a new podcast.
         *
         * @param  Request  $request
         * @return Response
         */
        public function store(Request $request)
        {
            // Create podcast...

            $job = (new ProcessPodcast($podcast))->onQueue('processing');

            dispatch($job);
        }
    }
**Dispatching To A Particular Connection**

If you are working with multiple queue connections, you may specify which connection to push a job to.
To specify the connection, use the ``onConnection`` method on the job instance:
::

    <?php

    namespace App\Http\Controllers;

    use App\Jobs\ProcessPodcast;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class PodcastController extends Controller
    {
        /**
         * Store a new podcast.
         *
         * @param  Request  $request
         * @return Response
         */
        public function store(Request $request)
        {
            // Create podcast...

            $job = (new ProcessPodcast($podcast))->onConnection('sqs');

            dispatch($job);
        }
    }

Of course, you may chain the ``onConnection`` and ``onQueue`` methods to specify the connection and the queue for a job:
::

    $job = (new ProcessPodcast($podcast))
                ->onConnection('sqs')
                ->onQueue('processing');
