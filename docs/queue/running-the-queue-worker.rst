Running The Queue Worker
========================

Laravel includes a queue worker that will process new jobs as they are pushed onto the queue.
You may run the worker using the ``queue:work`` Artisan command.
Note that once the ``queue:work`` command has started, it will continue to run until it is manually stopped or you close your terminal:
::

    php artisan queue:work

To keep the queue:work process running permanently in the background, you should use a process monitor such as Supervisor to ensure that the queue worker does not stop running.
Remember, queue workers are long-lived processes and store the booted application state in memory.
As a result, they will not notice changes in your code base after they have been started.
So, during your deployment process, be sure to restart your queue workers.

Specifying The Connection & Queue
---------------------------------

You may also specify which queue connection the worker should utilize.
The connection name passed to the work command should correspond to one of the connections defined in your  ``config/queue.php`` configuration file:
::

    php artisan queue:work redis

You may customize your queue worker even further by only processing particular queues for a given connection.
For example, if all of your emails are processed in an emails queue on your redis queue connection, you may issue the following command to start a worker that only processes only that queue:
::

    php artisan queue:work redis --queue=emails

Resource Considerations
-----------------------

Daemon queue workers do not "reboot" the framework before processing each job.
Therefore, you should free any heavy resources after each job completes.
For example, if you are doing image manipulation with the GD library, you should free the memory with imagedestroy when you are done.


Queue Priorities
----------------

Sometimes you may wish to prioritize how your queues are processed.
For example, in your  ``config/queue.php`` you may set the default queue for your ``redis`` connection to ``low``.
However, occasionally you may wish to push a job to a ``high`` priority queue like so:
::

    dispatch((new Job)->onQueue('high'));

To start a worker that verifies that all of the ``high`` queue jobs are processed before continuing to any jobs on the ``low`` queue, pass a comma-delimited list of queue names to the work command:
::

    php artisan queue:work --queue=high,low


Queue Workers & Deployment
--------------------------

Since queue workers are long-lived processes, they will not pick up changes to your code without being restarted.
So, the simplest way to deploy an application using queue workers is to restart the workers during your deployment process.
You may gracefully restart all of the workers by issuing the  ``queue:restart`` command:
::

    php artisan queue:restart

This command will instruct all queue workers to gracefully "die" after they finish processing their current job so that no existing jobs are lost.
Since the queue workers will die when the  ``queue:restart`` command is executed, you should be running a process manager such as Supervisor to automatically restart the queue workers.


Job Expirations & Timeouts
--------------------------

**Job Expiration**

In your config/queue.php configuration file, each queue connection defines a retry_after option.
This option specifies how many seconds the queue connection should wait before retrying a job that is being processed.
For example, if the value of ``retry_after`` is set to 90, the job will be released back onto the queue if it has been processing for 90 **seconds** without being deleted.
Typically, you should set the retry_after value to the maximum number of seconds your jobs should reasonably take to complete processing.

The only queue connection which does not contain a retry_after value is Amazon SQS.
SQS will retry the job based on the Default Visibility Timeout which is managed within the AWS console.
**Worker Timeouts**

The ``queue:work`` Artisan command exposes a ``--timeout`` option.
The ``--timeout`` option specifies how long the Laravel queue master process will wait before killing off a child queue worker that is processing a job.
Sometimes a child queue process can become "frozen" for various reasons, such as an external HTTP call that is not responding.
The ``--timeout`` option removes frozen processes that have exceeded that specified time limit:
::

    php artisan queue:work --timeout=60

The ``retry_after`` configuration option and the ``--timeout`` CLI option are different, but work together to ensure that jobs are not lost and that jobs are only successfully processed once.

The ``--timeout`` value should always be at least several seconds shorter than your  ``retry_after`` configuration value.
This will ensure that a worker processing a given job is always killed before the job is retried.
If your ``--timeout`` option is longer than your  ``retry_after`` configuration value, your jobs may be processed twice.
Worker Sleep Duration

When jobs are available on the queue, the worker will keep processing jobs with no delay in between them.
However, the sleep option determines how long the worker will "sleep" if there are no new jobs available:
::

    php artisan queue:work --sleep=3


