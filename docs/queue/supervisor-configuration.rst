Supervisor Configuration
========================

Installing Supervisor
---------------------

Supervisor is a process monitor for the Linux operating system, and will automatically restart your  ``queue:work`` process if it fails.
To install Supervisor on Ubuntu, you may use the following command:
::

    sudo apt-get install supervisor

If configuring Supervisor yourself sounds overwhelming, consider using Laravel Forge, which will automatically install and configure Supervisor for your Laravel projects.
Configuring Supervisor
----------------------

Supervisor configuration files are typically stored in the ``/etc/supervisor/conf.d`` directory.
Within this directory, you may create any number of configuration files that instruct supervisor how your processes should be monitored.
For example, let's create a ``laravel-worker.conf`` file that starts and monitors a queue:work process:
::

    [program:laravel-worker]
    process_name=%(program_name)s_%(process_num)02d
    command=php /home/forge/app.com/artisan queue:work sqs --sleep=3 --tries=3
    autostart=true
    autorestart=true
    user=forge
    numprocs=8
    redirect_stderr=true
    stdout_logfile=/home/forge/app.com/worker.log

In this example, the numprocs directive will instruct Supervisor to run 8 queue:work processes and monitor all of them, automatically restarting them if they fail.
Of course, you should change the  queue:work sqs portion of the command directive to reflect your desired queue connection.

Starting Supervisor
-------------------

Once the configuration file has been created, you may update the Supervisor configuration and start the processes using the following commands:
::

    sudo supervisorctl reread

    sudo supervisorctl update

    sudo supervisorctl start laravel-worker:*

For more information on Supervisor, consult the Supervisor documentation.


