Job Events
==========

Using the before and after methods on the Queue facade, you may specify callbacks to be executed before or after a queued job is processed.
These callbacks are a great opportunity to perform additional logging or increment statistics for a dashboard.
Typically, you should call these methods from a service provider.
For example, we may use the ``AppServiceProvider`` that is included with Laravel:
::

    <?php

    namespace App\Providers;

    use Illuminate\Support\Facades\Queue;
    use Illuminate\Support\ServiceProvider;
    use Illuminate\Queue\Events\JobProcessed;
    use Illuminate\Queue\Events\JobProcessing;

    class AppServiceProvider extends ServiceProvider
    {
        /**
         * Bootstrap any application services.
         *
         * @return void
         */
        public function boot()
        {
            Queue::before(function (JobProcessing $event) {
                // $event->connectionName
                // $event->job
                // $event->job->payload()
            });

            Queue::after(function (JobProcessed $event) {
                // $event->connectionName
                // $event->job
                // $event->job->payload()
            });
        }

        /**
         * Register the service provider.
         *
         * @return void
         */
        public function register()
        {
            //
        }
    }

Using the looping method on the Queue facade, you may specify callbacks that execute before the worker attempts to fetch a job from a queue.
For example, you might register a Closure to rollback any transactions that were left open by a previously failed job:
::

    Queue::looping(function () {
        while (DB::transactionLevel() > 0) {
            DB::rollBack();
        }
    });
