Connections Vs. Queues
======================

Before getting started with Laravel queues, it is important to understand the distinction between ``connections`` and ``queues``.
In your ``config/queue.php`` configuration file, there is a connections configuration option.
This option defines a particular connection to a backend service such as ``Beanstalk``, ``Amazon SQS``, ``Redis``.
However, any given queue connection may have multiple "queues" which may be thought of as different stacks or piles of queued jobs.

Note that each connection configuration example in the queue configuration file contains a queue attribute.
This is the default queue that jobs will be dispatched to when they are sent to a given connection.
In other words, if you dispatch a job without explicitly defining which queue it should be dispatched to, the job will be placed on the queue that is defined in the queue attribute of the connection configuration:
::

    // This job is sent to the default queue...
    dispatch(new Job);

    // This job is sent to the "emails" queue...
    dispatch((new Job)->onQueue('emails'));

Some applications may not need to ever push jobs onto multiple queues, instead preferring to have one simple queue.
However, pushing jobs to multiple queues can be especially useful for applications that wish to prioritize or segment how jobs are processed, since the Laravel queue worker allows you to specify which queues it should process by priority.
For example, if you push jobs to a high queue, you may run a worker that gives them higher processing priority:
::

    php artisan queue:work --queue=high,default

