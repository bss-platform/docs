Common
======

login
-----
::

    $scope.chatModel.user = {
        jid: 'nasterblue@nasterblue.ejabberd.com',
        password: "password",
        transports: ['websocket'],
        wsURL: 'ws://web.beesightsoft.com:5279/websocket'
    };
    $scope.chatService.xmppClient = XMPP.createClient($scope.chatModel.user);

    //login success
    $scope.chatService.xmppClient.on('auth:success', function (data) {
        $log.info('auth:success', data);
    });

    $scope.chatService.xmppClient.on('auth:failed', function () {
        $log.info('auth:failed');
    });

logout
------
::

    $scope.chatService.xmppClient.disconnect();

send a chat state
-----------------
::

    //['active', 'composing', 'paused', 'inactive', 'gone']
    var msg = {
        type: 'chat',
        to: scope.message.to,
        from: scope.message.from,
        chatState: chatState
    };
    scope.xmppClient.sendMessage(msg);

received a chat state
---------------------
::

    //['active', 'composing', 'paused', 'inactive', 'gone']
    $scope.chatService.xmppClient.on('chat:state', function (state) {
        $log.info('chat:state', state);
        if (state.from.bare === $scope.message.to) {

        } else {
            $scope.chatModel.userState.chatState = '';
        }
    });

send your presence
------------------
::

    $scope.chatService.xmppClient.sendPresence({
        caps: $scope.chatService.xmppClient.disco.caps
    });

receive chat presence
---------------------
::

    //['available', 'unavailable']
    $scope.chatService.xmppClient.on('presence', function (presence) {
        //update presence of jid
        if (presence.from.bare === $scope.chatModel.user.jid) {
            return false;
        }

        toastr.info($scope.chatModel.rosters[index].jid.local + ' is ' + presence.type + ' now', 'Hello', {
            closeButton: true,
            closeHtml: '<button></button>'
        });

        $scope.chatModel.chatStateNotifications[presence.from.bare] = presence;

    });


publish avatar
--------------
::

    var avatar = 'http://onwallhd.info/wp-content/uploads/pic-big/james-blunt_12.jpg';
    $scope.chatService.xmppClient.publishAvatar(jid, avatar, function (error, response) {
        $log.info(error, response);
    });

get avatar
----------
::

    $scope.chatService.xmppClient.getAvatar(jid, jid, function (error, response) {
        $log.info(error, response);
    });


publish vCard
-------------
::

    var vCard = {
                    website: "http://beesightsoft.com",
                    title: '',
                    description: '',
                    fullName: '',
                    nicknames: ''
                }
    $scope.chatService.xmppClient.publishVCard(vCard, function (error, response) {
        $log.info(error, response);
    });


get vCard
---------
::

    $scope.chatService.xmppClient.getVCard(jid, function (error, response) {
        $log.info(error, response);
    });
