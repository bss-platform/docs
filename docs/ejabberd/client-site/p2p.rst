P2P chat
========


send a subscription request to a jid
------------------------------------
::

    $scope.chatService.xmppClient.subscribe(jid);

received a subscription request
-------------------------------
::

    //on subscribe => jid received a subscription request
    $scope.chatService.xmppClient.on('subscribe', function (subscribe) {
        $log.info('subscribe', subscribe);
        toastr.info(subscribe.from.local + ' want to be your friend', 'Hello', {
            closeButton: true,
            closeHtml: '<button></button>'
        });
    });


accept subscription request
---------------------------
::

    $scope.chatService.xmppClient.acceptSubscription(jid);

deny subscription request
-------------------------
::

   $scope.chatService.xmppClient.denySubscription(jid);


fetch all roster(friends list)
------------------------------
::

    $scope.chatService.xmppClient.getRoster(function (err, resp) {
        $log.info(resp.roster.items);
    });


send a message
--------------
::

    //send message
    $scope.chatService.xmppClient.sendMessage({
        from: 'nasterblue@nasterblue.ejabberd.com',
        to: 'bss1@nasterblue.ejabberd.com',
        body: JSON.stringify({
            'replaced': false,
            'read': false,
            'body': 'Message content'
        }),
        requestReceipt: true
    });


incoming chat message
---------------------
::

    $scope.chatService.xmppClient.on('message', function (message) {
         if (!message.chatState) {
            if (message.body) {
                if (message.hasOwnProperty('mamItem')) {
                    //delayed chat message
                    var chat = message.mamItem.forwarded.message;
                } else {
                    //now chat message
                    var chat = message;
                }
            }
        }

        if (message.hasOwnProperty('receipt')) {

            //I just send a receipt
            $log.info('I just send a receipt', message);
        }

        if (message.hasOwnProperty('requestReceipt')) {

            //I have  read your receipt
        }
    });


outgoing chat message
---------------------
::

    $scope.chatService.xmppClient.on('message:sent', function (message) {
        if (!message.chatState) {

        }
    });

update read status of chat message
----------------------------------
::

    var confirmReceiveYourReceiptRequest = function (message) {
        var body = message.body;
        body.read = true;
        //or received
        $scope.chatService.xmppClient.sendMessage({
            to: 'nasterblue@nasterblue.ejabberd.com', //owner of this chat
            from: 'bss1@nasterblue.ejabberd.com',  //user that owner send to
            body: JSON.stringify(body),
            receipt: message.id,
            id: message.id
        });
    };

edit your chat message
----------------------
::

    //edit my chat
    var editMyMessage = function (message, newBodyChat) {
        var body = message.body;
        body.body = newBodyChat;
        body.replaced = true;
        $scope.chatService.xmppClient.sendMessage({
            from: 'nasterblue@nasterblue.ejabberd.com', //owner of this chat
            to: 'bss1@nasterblue.ejabberd.com', //user that owner send to
            replace: message.id,
            id: message.id,
            body: JSON.stringify(body)
        });
    };

remove your chat message
------------------------
::

    //remove my chat
    var removeMyMessage = function (message) {
        var body = message.body;
        if (body.body === '') {
            return false;
        }
        body.body = '';
        body.replaced = true;
        body.removed = true;
        $scope.chatService.xmppClient.sendMessage({
            from: 'nasterblue@nasterblue.ejabberd.com', //owner of this chat
            to: 'bss1@nasterblue.ejabberd.com', //user that owner send to
            replace: message.id,
            id: message.id,
            body: JSON.stringify(body)
        });
    };


fetch chat history with a jid
-----------------------------
::

    //get chat history
    return new Promise(function (resolve, reject) {
        $scope.chatService.xmppClient.searchHistory({
            'with': jid
        }, function (error, response) {
            if (error) {
                reject(error);
            } else {
                var history = _.map(response.mamResult.items, function (mam) {
                    var message = mam.forwarded.message;
                    message.time = mam.forwarded.delay.stamp;
                    if (angular.isString(message.body)) {
                        message.body = JSON.parse(message.body);
                    }

                    return message;
                });
                resolve(history);
            }

        });
    });


parse a chat message to my chat logs
------------------------------------
::

    //push to messages list
    var pushToChatLogs = function (message, jidHistoryIndex) {
        if (message.body) {

            if (message.hasOwnProperty('requestReceipt') && message.from.bare === $scope.chatModel.user.jid) {
                //owner of chat message
                message.isOwnerChat = true;
            } else {
                //other receivers
                message.isOwnerChat = false;
            }

            if (angular.isString(message.body)) {
                message.body = JSON.parse(message.body);
            }
            if (!$scope.chatModel.chatLogs[jidHistoryIndex]) {
                $scope.chatModel.chatLogs[jidHistoryIndex] = {};
            }
            if ($scope.chatModel.chatLogs[jidHistoryIndex][message.id]) {
                $scope.chatModel.chatLogs[jidHistoryIndex][message.id].body = message.body;

                //priority state of message
                if (message.body.removed) {
                    //owner remove this message

                    if (message.hasOwnProperty('replace')) {

                        $scope.chatModel.chatLogs[jidHistoryIndex][message.id].body = message.body;
                        $scope.chatModel.chatLogs[jidHistoryIndex][message.id].removedTime = message.time;

                    }

                } else if (message.body.replaced) {
                    //owner edited this message

                    if (message.hasOwnProperty('replace')) {
                        $scope.chatModel.chatLogs[jidHistoryIndex][message.id].body = message.body;
                        $scope.chatModel.chatLogs[jidHistoryIndex][message.id].replacedTime = message.time;
                    }

                }
                if (message.hasOwnProperty('receipt') && message.from.bare === $scope.chatModel.user.jid) {
                    //receiver read this message
                    if (message.body.read) {
                        //receiver read this message
                        $scope.chatModel.chatLogs[jidHistoryIndex][message.id].body = message.body;
                        $scope.chatModel.chatLogs[jidHistoryIndex][message.id].readTime = message.time;
                    }
                }

            } else {
                $scope.chatModel.chatLogs[jidHistoryIndex][message.id] = message;
            }

        }
    };