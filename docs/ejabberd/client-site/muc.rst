MUC chat
========

fetch all muc rooms by virtual host
-----------------------------------
::

   var room = 'conference.nasterblue.ejabberd.com';
    $scope.chatService.xmppClient.getDiscoItems(room, '', function (error, response) {
        if (response) {
           response.discoItems.items
        }

    });


join a muc room
---------------
::

    $scope.chatService.xmppClient.joinRoom(jid, $scope.message.nick, {
        status: 'This will be my nick ' + $scope.message.nick
    });


fetch all room members
----------------------
::

    //fetch room members
    var fetchRoomMembers = function (jid, forceFetch) {
        // roomMembers
        if ($scope.chatModel.roomMembers[jid]) {
            //exists
            if (forceFetch) {
                getRoomMembers(jid);
            }
        } else {
            getRoomMembers(jid);
        }

        function getRoomMembers(jid) {

            getRoomMembersBy(jid, 'member');
            getRoomMembersBy(jid, 'admin');
            getRoomMembersBy(jid, 'owner');

        }

        function getRoomMembersBy(jid, affiliation) {
            $scope.chatService.xmppClient.getRoomMembers(jid, {items: [{affiliation: affiliation}]}, function (err, res) {
                if (res && res.mucAdmin) {
                    var uniqueList = _.uniq(res.mucAdmin.items, function (item, key, a) {
                        return item.jid.bare;
                    });

                    if ($scope.chatModel.roomMembers[jid] === undefined) {
                        $scope.chatModel.roomMembers[jid] = [];
                    }
                    angular.merge($scope.chatModel.roomMembers[jid], uniqueList);

                    $scope.chatModel.roomMembers[jid] = _.uniq($scope.chatModel.roomMembers[jid], function (item, key, a) {
                        return item.jid.bare;
                    });
                }
            });
        }

    };

send a chat message to a room
-----------------------------
::

    //send message
    var sendMessage = function () {
        $scope.chatService.xmppClient.sendMessage({
            type: 'groupchat',
            from: 'nasterblue@nasterblue.ejabberd.com', //owner of chat
            to: 'nasterblue@conference.nasterblue.ejabberd.com', //room jid
            body: JSON.stringify({
                'replaced': false,
                'read': false,
                'body': 'Content chat'
            }),
            requestReceipt: true
        });
        $scope.message.body = '';

    };

update read status chat message
-------------------------------
::

    var confirmReceiveYourReceiptRequest = function (message) {
        var body = message.body;
        body.read = true;
        //or received
        $scope.chatService.xmppClient.sendMessage({
            type: 'groupchat',
            from: 'bss1@nasterblue.ejabberd.com', //member in room , not owner of chat
            to: 'nasterblue@conference.nasterblue.ejabberd.com', //room jid
            body: JSON.stringify(body),
            receipt: message.id,
            id: message.id
        });
    };

edit my chat message
--------------------
::

    //edit my chat
    var editMyMessage = function (message, newBodyChat) {
        var body = message.body;
        body.body = newBodyChat;
        body.replaced = true;
        $scope.chatService.xmppClient.sendMessage({
            type: 'groupchat',
            from: 'nasterblue@nasterblue.ejabberd.com',  //owner of chat
            to: 'nasterblue@conference.nasterblue.ejabberd.com', //room jid
            replace: message.id,
            id: message.id,
            body: JSON.stringify(body)
        });
    };

remove my chat message
----------------------
::

    //remove my chat
    var removeMyMessage = function (message) {
        var body = message.body;
        body.body = '';
        body.replaced = true;
        body.removed = true;

        $scope.chatService.xmppClient.sendMessage({
            type: 'groupchat',
            from: 'nasterblue@nasterblue.ejabberd.com',  //owner of chat
            to: 'nasterblue@conference.nasterblue.ejabberd.com', //room jid
            replace: message.id,
            id: message.id,
            body: JSON.stringify(body)
        });
    };


send chat state
---------------
Send chat state to all online member

parse a chat message to my chat logs
------------------------------------
::

    //push to messages list
    var pushToChatLogs = function (message, jidRomHistoryIndex) {
        //message.from.bare
        if (message.body) {
            if (message.hasOwnProperty('requestReceipt') && message.from.resource === $scope.chatModel.user.jid) {
                //owner of chat message
                message.isOwnerChat = true;
            } else {
                //other receivers
                message.isOwnerChat = false;
            }
            if (angular.isString(message.body)) {
                message.body = JSON.parse(message.body);
            }

            if (!$scope.chatModel.chatLogs[jidRomHistoryIndex]) {
                $scope.chatModel.chatLogs[jidRomHistoryIndex] = {};
            }
            if ($scope.chatModel.chatLogs[jidRomHistoryIndex][message.id]) {

                //priority state of message

                if (message.body.removed) {
                    //owner remove this message

                    if (message.hasOwnProperty('replace')) {
                        $scope.chatModel.chatLogs[jidRomHistoryIndex][message.id].body = message.body;
                        $scope.chatModel.chatLogs[jidRomHistoryIndex][message.id].removedTime = message.time;
                    }

                } else if (message.body.replaced) {
                    //owner edited this message

                    if (message.hasOwnProperty('replace')) {
                        $scope.chatModel.chatLogs[jidRomHistoryIndex][message.id].body = message.body;
                        $scope.chatModel.chatLogs[jidRomHistoryIndex][message.id].replacedTime = message.time;
                    }

                }
                if (message.hasOwnProperty('receipt') && message.from.resource === $scope.chatModel.user.jid) {
                    //receiver read this message
                    if (message.body.read) {
                        //receiver read this message
                        $scope.chatModel.chatLogs[jidRomHistoryIndex][message.id].body = message.body;
                        $scope.chatModel.chatLogs[jidRomHistoryIndex][message.id].readTime = message.time;
                    }
                }


            } else {
                $scope.chatModel.chatLogs[jidRomHistoryIndex][message.id] = message;
            }

            $scope.chatService.fetchAvatarUser(message.from.resource);
        }

    };