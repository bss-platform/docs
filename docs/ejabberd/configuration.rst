Configuration
=============

authentication
--------------
::

    auth_method: sql


publish admin api
-----------------

``Admin APIs`` : https://docs.ejabberd.im/developer/ejabberd-api/admin-api

::

    commands_admin_access: configure
    commands:
      add_commands:
        - send_message
        - status
        - get_roster
        - register
        - check_account
        - get_offline_count
        - create_room
        - change_room_option
        - send_direct_invitation
        - destroy_room


rest api http/websocket/http-bind
---------------------------------
::

  -
    port: 52xx
    module: ejabberd_http
    request_handlers:
      "/websocket": ejabberd_http_ws
      "/log": mod_log_http
      "/api": mod_http_api
    ##  "/pub/archive": mod_http_fileserver
    web_admin: true
    http_bind: true
    ## register: true
    captcha: false

commandline
-----------
``Multi User Chat Administration Commands`` : https://docs.ejabberd.im/admin/ejabberdctl/muc-admin/

Example:

*create user
::

    ejabberdctl register nasterblue  nasterblue.ejabberd.com password


*create room
::

    ejabberdctl create_room nasterblue  conference.nasterblue.ejabberd.com nasterblue.ejabberd.com

**destroy room
::

    ejabberdctl  destroy_room nasterblue conference.nasterblue.ejabberd.com

