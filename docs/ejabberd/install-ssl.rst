Install Ejabberd SSL
====================

Docker compose file
-------------------

Create ``/env/common.env`` file include :
::

    #COMMON
    ERLANG_NODE=ejabberd
    EJABBERD_HTTPS=true
    EJABBERD_PROTOCOL_OPTIONS_TLSV1=true
    EJABBERD_PROTOCOL_OPTIONS_TLSV1_1=true

    ##VHOST AND ACCOUNT
    XMPP_DOMAIN=project-domain.domain
    EJABBERD_ADMINS=admin@project-domain.domain
    EJABBERD_USERS=admin@project-domain.domain:HoangGia3116  bss1@project-domain.domain:password bss2@project-domain.domain:password



Create ``/env/common.module`` file include :
::

    ## MODULES
    EJABBERD_MOD_OAUTH_ENABLE=true
    EJABBERD_MOD_OAUTH_PREFIX=/oauth
    EJABBERD_MOD_HTTP_API_ENABLE=true
    EJABBERD_MOD_HTTP_API_PREFIX=/api/v1
    EJABBERD_MOD_MUC_ADMIN=true
    EJABBERD_MOD_ADMIN_EXTRA=true
    EJABBERD_REGISTER_ADMIN_ONLY=true
    EJABBERD_MOD_MAM=true
    EJABBERD_SKIP_MODULES_UPDATE=false
    EJABBERD_RESTART_AFTER_MODULE_INSTALL=true



Create ``/env/mysql.module`` file include :
::

    ## MYSQL
    EJABBERD_AUTH_METHOD=sql
    EJABBERD_CONFIGURE_SQL=true
    EJABBERD_DEFAULT_DB=sql
    EJABBERD_SESSION_DB=sql
    EJABBERD_SQL_TYPE=mysql
    EJABBERD_SQL_SERVER=128.199.94.240
    EJABBERD_SQL_DATABASE=ejabberd
    EJABBERD_SQL_USERNAME=root
    EJABBERD_SQL_PASSWORD=hoanggia3116
    EJABBERD_SQL_POOL_SIZE=10
    EJABBERD_SQL_PORT=3306

Create ``ssl`` folder include file:
::

    project-domain.domain.pem
    host.pem


Create ``docker-compose.yml`` file include :
* use mysql db
* ejabberd admin page
* port expose
       - 5222
       - 5269
       - 5280
       - 4560
       - 5443

::

    version: '2'
    services:
      ejabberd-service:
        container_name: beesightsoft-ejabberd-service
        image: beesightsoft/debian-ejabberd:16.04
        ports:
           - 5222:5222
           - 5269:5269
           - 5280:5280
           - 4560:4560
           - 5443:5443
        env_file:
          - ./env/common.env
          - ./env/mysql.env
          - ./env/module.env
        volumes:
          - <real ssl folder path>:/opt/ejabberd/ssl

Run docker compose
------------------
::

    docker-composer up -d


Ejabberd Admin
--------------
::

    https://project-domain.domain:5280/admin

.. image:: https://docs.ejabberd.im/static/images/admin/webadmin.png

Port
----
* 4560 (XMLRPC)
* 5222 (Client 2 Server)
* 5269 (Server 2 Server)
* 5280 (HTTP admin/websocket/http-bind)
* 5443 (HTTP Upload)