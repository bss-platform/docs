Intro
=====

ejabberd is a Rock Solid, Massively Scalable, Infinitely Extensible ``XMPP`` Server

ejabberd Modules Development
----------------------------

.. image::  https://docs.ejabberd.im/static/images/architect/ejabberd_internals.png


.. image::  https://docs.ejabberd.im/static/images/architect/ejabberd_large_scale.png

References
----------
* https://docs.ejabberd.im/
