Integrates
==========

Backend integration
-------------------

* REST API : https://docs.ejabberd.im/developer/ejabberd-api/
* ejabberdctl command-line tool


Web library with WebSocket support and fallback to BOSH support:
----------------------------------------------------------------
* stanza.io : https://github.com/legastero/stanza.io



