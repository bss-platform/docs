MUC
===

Configuration
-------------

- Docs : https://docs.ejabberd.im/admin/configuration/#mod-muc

- Example for public room
::

  mod_muc:
    ## host: "conference.@HOST@"
    access:
      - allow
    access_admin:
      - allow: all
    access_create: muc_create
    access_persistent: muc_create
    history_size: 0
    max_user_conferences: 100000
    default_room_options:
      mam: true
      persistent: true
      members_by_default: true
      allow_query_users: true
      allow_private_messages: true
      allow_private_messages_from_visitors: true
      public: true

- Example for private room
::

  mod_muc:
    ## host: "conference.@HOST@"
    access:
      - allow
    access_admin:
      - allow: admin
    access_create: muc_create
    access_persistent: muc_create
    history_size: 0
    max_user_conferences: 100000
    default_room_options:
      mam: true
      persistent: true
      members_only: true
      members_by_default: true
      allow_query_users: false
      allow_private_messages: true
      allow_private_messages_from_visitors: false
      public: false


Fetch discovery rooms
---------------------
- **Private room** :
+ Only ``owner`` or ``admin`` or ``member`` affilication can
- **Public room** :
+ All users can

Fetch room members/ room occupants
----------------------------------
- **Private room** :
+ Only ``owner`` or ``admin`` affilication can
+ Fetch all room members by ``role: 'participant'``

- **Public room** :
+ All room participants can
+ Fetch all room members by ``role: 'moderator'``


Room with options
-----------------
- **Public room**
+ Example: Set room to public by calling ``change_room_option`` API
::

    {
        "name" : "nasterblue",
        "service" : "conference.192.168.99.100",
        "option" : "public",
        "value" : "true"
    }

+ Users can discovery public room
+ Users can not enter public room : ``Only occupants are allowed to send messages to the conference``
+ Users want to enter public room and chat, need to change options ``members_only`` to ``false`` && ``members_by_default`` to ``true`` by calling ``change_room_option`` API
Example::
::

    {
        "name" : "nasterblue",
        "service" : "conference.192.168.99.100",
        "option" : "members_only",
        "value" : "false"
    }

    {
        "name" : "nasterblue",
        "service" : "conference.192.168.99.100",
        "option" : "members_by_default",
        "value" : "true"
    }


+ Users want to fetch current occupants in room, need to set ``affiliation`` to ``admin`` or ``owner`` by calling ``set_room_affiliation`` API
Example
::

    {
        "name" : "nasterblue",
        "service" : "conference.192.168.99.100",
        "jid": "nasterblue@192.168.99.100",
        "affiliation": "admin"
    }

+ User can send chat state to these occupants in room so other occupants can see you are typing

- **Private room**
+ Example: Set room to private by calling ``change_room_option`` API
::

    {
        "name" : "nasterblue",
        "service" : "conference.192.168.99.100",
        "option" : "public",
        "value" : "false"
    }


+ Users can not discovery private room
+ Users want to discovery private room, enter room and chat, need to set ``affiliation`` to ``member`` by calling ``set_room_affiliation`` API
Example
::

    {
        "name" : "nasterblue",
        "service" : "conference.192.168.99.100",
        "jid": "nasterblue@192.168.99.100",
        "affiliation": "member"
    }

+ Users want to discovery private room, enter room , chat and fetch current occupants in room, need to set ``affiliation`` to ``admin`` or ``owner`` by calling ``set_room_affiliation`` API
Example
::

    {
        "name" : "nasterblue",
        "service" : "conference.192.168.99.100",
        "jid": "nasterblue@192.168.99.100",
        "affiliation": "admin"
    }

