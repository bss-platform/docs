Admin APIs
==========

Link
----
https://docs.ejabberd.im/developer/ejabberd-api/admin-api/


Basic access authentication
---------------------------
- The Authorization field is constructed as follows:[6]

+The username and password are combined with a single colon. (:)
+The resulting string is encoded into an octet sequence.[7]
+The resulting string is encoded using a variant of Base64.[8]
+The authorization method and a space is then prepended to the encoded string, separated with a space (e.g. "Basic ").

::

    Authorization: Basic QWxhZGRpbjpPcGVuU2VzYW1l


**Example**


register
--------
::

    http://web.beesightsoft.com:5279/api/v1/register
    {
        "user" : "nasterblue",
        "host" : "nasterblue.ejabberd.com",
        "password" : "password"
    }

change_password
---------------
::

    http://web.beesightsoft.com:5279/api/v1/change_password
    {
        "user" : "nasterblue",
        "host" : "nasterblue.ejabberd.com",
        "newpass" : "newpass"
    }


create_room
-----------
::

    http://web.beesightsoft.com:5279/api/v1/create_room
    {
      "name" : "nasterblue",
      "service" : "conference.nasterblue.ejabberd.com",
      "host" : "nasterblue.ejabberd.com"
    }

destroy_room
-----------
::

    http://web.beesightsoft.com:5279/api/v1/destroy_room
    {
      "name" : "nasterblue",
      "service" : "conference.nasterblue.ejabberd.com",
      "host" : "nasterblue.ejabberd.com"
    }

set_room_affiliation
--------------------
::

    http://web.beesightsoft.com:5279/api/v1/set_room_affiliation
    {
      "name" : "nasterblue",
      "service" : "conference.nasterblue.ejabberd.com",
      "jid": "bss9@nasterblue.ejabberd.com",
      "affiliation": "admin"
    }


change_room_option
------------------
::

    http://web.beesightsoft.com:5279/api/v1/change_room_option
    {
      "name" : "nasterblue",
      "service" : "conference.nasterblue.ejabberd.com",
      "option" : "public",
      "value" : "true"
    }


change_room_option
------------------
::

    http://web.beesightsoft.com:5279/api/v1/change_room_option
    {
      "name" : "nasterblue",
      "service" : "conference.nasterblue.ejabberd.com"
    }


get_room_affiliations
---------------------
::

    http://web.beesightsoft.com:5279/api/v1/get_room_affiliations
    {
      "name" : "nasterblue",
      "service" : "conference.nasterblue.ejabberd.com"
    }


