Authentication
==============


Sentinel::authenticate($credentials,$remember)
----------------------------------------------

This method authenticates a user against the given ``$credentials``,
additionally a second bool argument of true can be passed to set the remember state on the user.

Returns: ``Cartalyst\Sentinel\Users\UserInterface`` or ``false``.
::

    $remember= true;
    $credentials = [
        'email'    => 'john.doe@example.com',
        'password' => 'password',
    ];

    Sentinel::authenticate($credentials,$remember);


Sentinel::authenticateAndRemember($credentials)
-----------------------------------------------

This method authenticates and remembers the user, it's an alias fore the ``authenticate()`` method
but it sets the ``$remember`` flag to ``true``.

Returns: ``Cartalyst\Sentinel\Users\UserInterface`` or ``false``.
::

    $credentials = [
        'email'    => 'john.doe@example.com',
        'password' => 'password',
    ];

    Sentinel::authenticateAndRemember($credentials);


