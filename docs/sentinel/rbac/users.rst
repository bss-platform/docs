Users
=====

Sentinel::findById($id)
-----------------------
::

    $user = Sentinel::findById(1);

Sentinel::findByCredentials($credentials)
-----------------------------------------
::

    $credentials = [
        'login' => 'john.doe@example.com',
    ];

    $user = Sentinel::findByCredentials($credentials);


