Checking for Permissions
========================

hasAccess
---------

This method will strictly require all passed permissions to be true in order to grant access.

This test will require both ``user.create`` and ``user.update`` to be true in order for permissions to be granted.
::

    $user = Sentinel::findById(1);

    if ($user->hasAccess(['user.create', 'user.update']))
    {
        // Execute this code if the user has permission
    }
    else
    {
        // Execute this code if the permission check failed
    }


hasAnyAccess
------------

This method will grant access if any permission passes the check.

This test will require only one permission of ``user.admin`` and ``user.create`` to be true in order for permissions to be granted.
::

    if (Sentinel::hasAnyAccess(['user.admin', 'user.update']))
    {
        // Execute this code if the user has permission
    }
    else
    {
        // Execute this code if the permission check failed
    }


* Note You can use ``Sentinel::hasAccess()`` or ``Sentinel::hasAnyAccess()`` directly which will call the methods on the currently logged in user, incase there's no user logged in, a ``BadMethodCallException`` will be thrown.

Wildcard Checks
---------------
Permissions can be checked based on wildcards using the ``*`` character to match any of a set of permissions.
::

    $user = Sentinel::findById(1);

    if ($user->hasAccess('user.*'))
    {
        // Execute this code if the user has permission
    }
    else
    {
        // Execute this code if the permission check failed
    }


Controller Based Permissions
----------------------------

You can easily implement permission checks based on controller methods, consider the following example implemented as a Laravel filter.

Permissions can be stored as action names on users and roles, then simply perform checks on the action before executing it and redirect on failure with an error message.

::

    Route::filter('permissions', function($route, $request)
    {
        $action = $route->getActionName();

        if (Sentinel::hasAccess($action))
        {
            return;
        }

        return Redirect::to('/')->withErrors('Permission denied.');
    });
