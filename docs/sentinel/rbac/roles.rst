Roles
=====

Create a new role
-----------------
::

    $role = Sentinel::getRoleRepository()->createModel()->create([
        'name' => 'Subscribers',
        'slug' => 'subscribers',
    ]);


Assign a user to a role
-----------------------
::

    $user = Sentinel::findById(1);

    $role = Sentinel::findRoleByName('Subscribers');

    $role->users()->attach($user);


Remove a user from a role
-------------------------
::

    $user = Sentinel::findById(1);

    $role = Sentinel::findRoleByName('Subscribers');

    $role->users()->detach($user);


