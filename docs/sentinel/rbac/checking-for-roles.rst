Checking for roles
==================
Check if the current user belongs to the given role.

Sentinel::inRole($role)
-----------------------

::

    $admin = Sentinel::inRole('admin');

