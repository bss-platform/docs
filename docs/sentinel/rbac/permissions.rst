Permissions
===========

Permissions live on permissible models, users and roles.

You can add, modify, update or delete permissions directly on the objects.


Storing Permissions
-------------------

Grant the **user** ``user.create`` and reject ``user.delete`` permissions.
::

    $user = Sentinel::findById(1);

    $user->permissions = [
        'user.create' => true,
        'user.delete' => false,
    ];

    $user->save();


or

::

    $user = Sentinel::findById(1);

    $user->addPermission('user.create');
    $user->addPermission('user.delete', false);

    $user->save();

or
::

    $user = Sentinel::findById(1);

    $user->removePermission('user.delete')->save();



Grant the **role** ``user.update`` and ``user.view`` permissions.
::

    $role = Sentinel::findRoleById(1);

    $role->permissions = [
        'user.update' => true,
        'user.view' => true,
    ];

    $role->save();


or
::

    $role = Sentinel::findRoleById(1);

    $role->updatePermission('user.update');
    $role->updatePermission('user.update', false, true)->save();

* Note 1: ``addPermission``, ``updatePermission`` and ``removePermission`` are chainable.
* Note 2: On ``updatePermission``, passing true as a third argument will create the permission if it does not already exist.

