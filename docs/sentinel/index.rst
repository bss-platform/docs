Sentinel
========

.. toctree::
    :maxdepth: 4
    :glob:

    registration
    authentication
    rbac/index


References
----------
* https://cartalyst.com/manual/sentinel/2.0
