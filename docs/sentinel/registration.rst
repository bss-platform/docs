Registration
============

Sentinel::register($credentials,$active)
----------------------------------------

The first argument is a key/value pair which should contain the user login column name, the password and other attributes you see fit.

The second argument is a boolean, that when set to true will automatically activate the user account.
::

    $active = true;
    $credentials = [
        'email'    => 'john.doe@example.com',
        'password' => 'password',
    ];

    $user = Sentinel::register($credentials,$active);


Sentinel::registerAndActivate($credentials)
-------------------------------------------

This method registers and activates the user, it's an alias for the ``register()`` method but it sets the ``$active`` flag to true.
::

    $credentials = [
        'email'    => 'john.doe@example.com',
        'password' => 'password',
    ];

    $user = Sentinel::registerAndActivate($credentials);

