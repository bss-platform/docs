Widgets
=======

Widgets are pieces of views which can be re-used throughout your Platform 5 application.
An example could be a menu widget for your custom navigation or a Twitter feed widget.
Widgets can both be created directly into your app in the ``app/widgets`` folder or through extensions.


.. toctree::
    :maxdepth: 4
    :glob:

    creating-widgets
    registering-widgets
    using-widgets




