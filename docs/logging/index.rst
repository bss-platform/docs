Logging
=======

Requirement
-----------
- rap2hpoutre/laravel-log-viewer : https://github.com/rap2hpoutre/laravel-log-viewer
- logentries/logentries-monolog-handler : https://github.com/LogentriesCommunity/logentries-monolog-handler

Update composer.json
--------------------
::

    "repositories": [
        {
            "url": "git@bitbucket.org:bss-vendor/log.git",
            "type": "git"
        }
    ],
    "require": {
        "core/log": "dev-master"
    }


Update composer
---------------
::

    composer update


Or use log file system
----------------------
::

    php artisan vendor:publish \
      --provider="Rap2hpoutre\LaravelLogViewer\LaravelLogViewerServiceProvider" \
      --tag=views

**Admin pannel**
.. image:: figure-2.png

Or use Logentries adapter
-------------------------
Update Logentries Service Provider in ``config/app.php``

::

    'providers' => [
        //Enable Logentries
        Core\Log\Providers\LogentriesServiceProvider::class,


**Admin pannel**
.. image:: figure-1.png

Environment
-----------
::
    APP_LOGENTRIES_TOKEN=xxx


Usage
-----
Same as laravel log


