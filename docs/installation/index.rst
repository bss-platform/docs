Installation
============

Generate ssh keys
-----------------
* Open Gitbash
.. image:: gitbash.png

* Generate ssh keys

::

    ssh-keygen -t rsa -C "xxx@beesightsoft.com"

.. image:: generate-sshkey.png

* Send ssh public key to **Naster Blue** and waiting for him until he added into github/bitbucket account. But he is extremely busy now.

Create default user and database name
-------------------------------------
* Create user **forge** without password & database name **forge**
* User phpmyadmin
.. image:: create-user-database-name.png

* User mysql commandline

::

    CREATE USER 'forge'@'localhost' IDENTIFIED VIA mysql_native_password USING '***';GRANT ALL PRIVILEGES ON *.* TO 'forge'@'localhost' REQUIRE NONE WITH GRANT OPTION MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;CREATE DATABASE IF NOT EXISTS `forge`;GRANT ALL PRIVILEGES ON `forge`.* TO 'forge'@'localhost';GRANT ALL PRIVILEGES ON `forge\_%`.* TO 'forge'@'localhost';

Clone the platform
------------------
::

    git clone git@bitbucket.org:bss-vendor/platform.git --branch=master  platform
    cd platform
    composer install


Install the platform
--------------------
::

     chmod +x install.sh
     ./install.sh



Development
-----------
::

    php artisan theme:publish --extensions --watch
    php artisan queue:listen --sleep=3 --tries=3  --timeout=120

Update swagger ui config
------------------------
- Config file `config/latrell-swagger.php` is the primary way you interact with Swagger.

+ ``paths`` : the primary project extension, example : base_path('extensions/demo')
+ ``default-base-path`` : the domain name server, example : `http://bss-core.dev/`

- Example

::

    <?php
    return array(
        'enable' => config('app.debug'),

        'prefix' => 'api-docs',

        'paths' => base_path('extensions/{projectName}'),
        'output' => storage_path('swagger/docs'),
        'exclude' => null,
        'default-base-path' => 'http://bss-core.dev/',
        'default-api-version' => null,
        'default-swagger-version' => null,
        'api-doc-template' => null,
        'suffix' => '.{format}',

        'title' => 'Swagger UI'
    );


Demo extension
--------------
* Update composer.json

::

    "require": {
        "core/demo": "dev-master"
    }


* Update composer

::

    composer update

* Update autoload extension and run migrate

::

    php artisan extension:autoload --extension=core/demo
    php artisan migrate

* Open swagger ui link

::

    http://bss-core.dev/api-docs

Wiki
----
- [Latest](http://docs.ibss.io/docs/core/en/latest/index.html)