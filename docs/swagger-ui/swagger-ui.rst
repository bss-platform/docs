Install
=======

Update composer.json
--------------------
::

    'repositories': [
        {
            'url': 'git@bitbucket.org:bss-vendor/vendor-swagger-ui.git',
            'type': 'git'
        }
    ],
    'require': {
        'core-vendor/swagger-ui': 'dev-master'
    }


Update composer
---------------
::

    composer update

Register Swagger service provider
---------------------------------
Register the Swagger service provider by adding it to the providers array  in the `app/config/app.php` file.
::

    'providers' => [
        ...

       //Swagger
       'Latrell\Swagger\SwaggerServiceProvider',



Publish configuration
---------------------
::

    php artisan vendor:publish

    Copied File [/vendor/core-vendor/swagger-ui/src/config/config.php] To [/config/latrell-swagger.php]
    Publishing complete.


Update configuration
--------------------
- Config file `config/latrell-swagger.php` is the primary way you interact with Swagger.
*  ``paths`` : the primary project extension, example : base_path('extensions/tacko')
*  ``default-base-path`` : the domain name server, example : `http://bss-core.dev/`

- Example
::

    return array(
        'enable' => config('app.debug'),

        'prefix' => 'api-docs',

        'paths' => base_path('extensions/{projectName}'),
        'output' => storage_path('swagger/docs'),
        'exclude' => null,
        'default-base-path' => 'http://bss-core.dev/',
        'default-api-version' => null,
        'default-swagger-version' => null,
        'api-doc-template' => null,
        'suffix' => '.{format}',

        'title' => 'Swagger UI'
    );

Example
=======

* [Demo](http://petstore.swagger.wordnik.com)
* [Documentation](http://zircote.com/swagger-php)
