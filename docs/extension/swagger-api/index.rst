Swagger UI
==========


- The swagger ui include crud
.. image:: ../scaffold-api/figure-15.png

- Pagination api
.. image:: ../scaffold-api/figure-16.png

- Create  api
.. image:: ../scaffold-api/figure-17.png

- Delete api
.. image:: ../scaffold-api/figure-18.png

- Update api
.. image:: ../scaffold-api/figure-19.png
