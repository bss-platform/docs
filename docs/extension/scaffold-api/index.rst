Scaffold APIs
=============

- Components
.. image:: figure-1.png

- Migration
.. image:: figure-2.png

- Notify to update autoload composer
.. image:: figure-3.png

- Run command to update autoload composer and migrate the migration
.. image:: figure-4.png

- Api Controller include swagger ui and crud
.. image:: figure-5.png

- Criteria
.. image:: figure-6.png

- Eloquent model include transform
.. image:: figure-7.png

- Presenter for another eloquent model transform
.. image:: figure-8.png

- Transform for the Presenter
.. image:: figure-11.png

- Service provider
.. image:: figure-9.png

- Repository
.. image:: figure-10.png

- Automatic update service provider and routes
.. image:: figure-12.png

- New api roues
.. image:: figure-13.png

- The migration
.. image:: figure-14.png

- The swagger ui include crud
.. image:: figure-15.png

- Pagination api
.. image:: figure-16.png

- Create  api
.. image:: figure-17.png

- Delete api
.. image:: figure-18.png

- Update api
.. image:: figure-19.png