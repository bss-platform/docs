Update user & group for server PHP
==================================

``Apache`` server
-----------------
- Add user to ``daemon`` group
- Example for OSX
::

    sudo dseditgroup -o edit -a nasterblue -t user daemon
- Edit  `httpd.conf`
::

    User nasterblue
    Group daemon

``Nginx`` server
----------------
- Add user to ``www`` group
- Example for OSX
::

    sudo dseditgroup -o edit -a nasterblue -t user www

- Edit `nginx.conf` set
::

     user to nasterblue www;

Restart  PHP server
-------------------


Create new extension
====================
- Figure 1
.. image:: extension-images/figure-1.png

- Figure 2
.. image:: extension-images/figure-2.png

- Figure 3
.. image:: extension-images/figure-3.png

- Figure 4
.. image:: extension-images/figure-4.png

- Figure 5
.. image:: extension-images/figure-5.png

- Figure 6
.. image:: extension-images/figure-6.png

- Figure 7
.. image:: extension-images/figure-7.png


Add new component for extension
===============================

.. image:: component-images/figure-1.png
