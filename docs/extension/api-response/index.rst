Response APIs
=============
Inject Response contract into a php class
-----------------------------------------
``MyOverrides\Traits\Api\Response``
Example

::

    <?php

    namespace MyOverrides\Controllers\Api;

    use Illuminate\Foundation\Bus\DispatchesJobs;
    use Illuminate\Routing\Controller as Controller;
    use Illuminate\Foundation\Validation\ValidatesRequests;
    use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
    use MyOverrides\Traits\Api;

    abstract class BaseController extends Controller
    {
        use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

        use Api\Response;

    }

Success
-------

* respondWithSuccess($data)
+ $data is array
+ Example

::

     $demoproduct = $this->demoproducts->findById($id);
     if(empty($demoproduct)){
           return $this->errorNotFound();
     }
     return $this->respondWithSuccess($demoproduct);

* respondWithPaging($data)
+ or $data instanceof \Illuminate\Support\Collection
+ or $data instanceof \Illuminate\Pagination\LengthAwarePaginator
Example

::

    $input = $this->parsedBody;
    $this->demoproducts->pushCriteria(new Demoproduct());
    $demoproduct = $this->demoproducts->paginate($input['pageSize']);
    return $this->respondWithPaging($demoproduct);




Error
-----
* Where to define error code and error messages : `resources/lang/en/error-code-messages.php`
- File defined error code and message
.. image:: figure-1.png


* respondWithErrorKey($errorKey, $statusCode = 400, $messageOverride = '', array $headers = [])
Example

::

    return $this->respondWithErrorKey('system.wrong_arguments');

* respondWithErrorKeys(array $errorKeys, $statusCode = 400, array $headers = [])
Example

::

    return $this->respondWithErrorKeys(['system.wrong_arguments','system.permission']);

* errorInternalError($message = 'Internal Error')
Example

::

    return $this->errorInternalError();

* errorNotFound($message = 'Resource Not Found')
Example

::

    return $this->errorNotFound();

* errorWrongArgs($message = 'Wrong Arguments')
Example

::

    return $this->errorNotFound();

* errorForbidden($message = 'Forbidden')
Example

::

    return $this->errorNotFound();

* errorNotImplemented($message = 'Not implemented')
Example

::

    return $this->errorNotFound();

* errorByUserBlock()
Example

::

    return $this->errorByUserBlock();

* errorUnauthorized($message = 'Unauthorized')
Example

::

    return $this->errorUnauthorized();

* errorTokenExpired($message = 'Token expired')
Example

::

    return $this->errorTokenExpired();

* errorTokenRequired($message = 'Token required')
Example

::

    return $this->errorTokenRequired();



