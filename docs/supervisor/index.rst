Supervisor
==========

Install
-------
http://supervisord.org/installing.html


Configuration File
------------------
**Configuration File** : ``/etc/supervisord.conf``

**Example**

::

    [unix_http_server]
    file=/var/run/supervisor.sock   ; (the path to the socket file)
    chmod=0700                       ; sockef file mode (default 0700)

    [inet_http_server]         ; inet (TCP) server disabled by default
    port=127.0.0.1:5488        ; (ip_address:port specifier, *:port for all iface)

    [supervisord]
    nodaemon=true               ; (start in foreground if true;default false)
    logfile=/var/log/supervisor/supervisord.log
    pidfile=/var/run/supervisord.pid
    childlogdir=/var/log/supervisor

    [rpcinterface:supervisor]
    supervisor.rpcinterface_factory = supervisor.rpcinterface:make_main_rpcinterface

    [supervisorctl]
    serverurl=http://127.0.0.1:5488 ; use an http:// url to specify an inet socket

    [include]
    files = /etc/supervisor/conf.d/*.conf

Restart Supervisor
------------------
::

    sudo unlink /tmp/supervisor.sock
    supervisorctl restart


Search pattern
--------------
::

    [ec2-user@ip-xxx ~]$  ps -ef | grep supervisord
    ec2-user 20548     1  0 03:10 ?        00:00:00 /usr/bin/python2.7 /usr/local/bin/supervisord
    ec2-user 21314 19905  0 03:16 pts/1    00:00:00 grep --color=auto supervisord

Run into Supervisor command line
--------------------------------
**List all process**

::

    [ec2-user@ip-172-30-0-69 ~]$ supervisorctl
    laravel-worker:laravel-worker_00   RUNNING   pid 20724, uptime 0:08:35
    laravel-worker:laravel-worker_01   RUNNING   pid 20725, uptime 0:08:35
    laravel-worker:laravel-worker_02   RUNNING   pid 20726, uptime 0:08:35
    laravel-worker:laravel-worker_03   RUNNING   pid 20727, uptime 0:08:35
    laravel-worker:laravel-worker_04   RUNNING   pid 20728, uptime 0:08:35
    laravel-worker:laravel-worker_05   RUNNING   pid 20729, uptime 0:08:35
    laravel-worker:laravel-worker_06   RUNNING   pid 20730, uptime 0:08:35
    laravel-worker:laravel-worker_07   RUNNING   pid 20731, uptime 0:08:35


**List all commands to use with process**

::

    ec2-user@ip-172-30-0-69 ~]$ supervisorctl
    supervisor> help
    default commands (type help <topic>):
    =====================================
    add    exit      open  reload  restart   start   tail
    avail  fg        pid   remove  shutdown  status  update
    clear  maintail  quit  reread  signal    stop    version


**Example prints the last 10 lines of a output process**

::

    ec2-user@ip-172-30-0-69 ~]$ supervisorctl
    supervisor> tail -f laravel-worker:laravel-worker_00
    ==> Press Ctrl-C to exit <==


