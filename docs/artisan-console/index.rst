Artisan Console
===============

Console Commands
----------------

Artisan is the command-line interface included with Laravel. It provides a number of helpful commands that can assist you while you build your application. To view a list of all available Artisan commands, you may use the list command:
::

    php artisan list

Every command also includes a "help" screen which displays and describes the command's available arguments and options. To view a help screen, simply precede the name of the command with help:
::

    php artisan help migrate

Task Scheduling
---------------

In the past, you may have generated a Cron entry for each task you needed to schedule on your server.
However, this can quickly become a pain, because your task schedule is no longer in source control and you must SSH into your server to add additional Cron entries.

Laravel's command scheduler allows you to fluently and expressively define your command schedule within Laravel itself.
When using the scheduler, only a single Cron entry is needed on your server.
Your task schedule is defined in the ``app/Console/Kernel.php`` file's schedule method.  To help you get started, a simple example is defined within the method.

Starting The Scheduler

When using the scheduler, you only need to add the following Cron entry to your server.
If you do not know how to add Cron entries to your server, consider using a service such as Laravel Forge which can manage the Cron entries for you:
::

    * * * * * php /path/to/artisan schedule:run >> /dev/null 2>&1

This Cron will call the Laravel command scheduler every minute.
When the ``schedule:run`` command is executed, Laravel will evaluate your scheduled tasks and runs the tasks that are due.


References
----------

* https://laravel.com/docs/5.3/artisan
* https://laravel.com/docs/5.3/scheduling
