Eloquent Repository
===================


Abstract BaseRepository
-----------------------
``MyOverrides\Repository\Eloquent\BaseRepository``

Methods
-------

- MyOverrides\RepositoryContracts\RepositoryInterface

::

    /**
     * Retrieve data array for all
     *
     * @param array $columns
     * @return \Illuminate\Support\Collection|array
     */
    public function all($columns = ['*']);

    /**
     * Retrieve first data of repository
     *
     * @param array $columns
     *
     * @return mixed
     */
    public function first($columns = ['*']);

    /**
     * Retrieve data array for populate field select
     *
     * @param string $column
     * @param string|null $key
     *
     * @return \Illuminate\Support\Collection|array
     */
    public function lists($column, $key = null);


    /**
     * Find data by id
     *
     * @param       $id
     * @param array $columns
     *
     * @return mixed
     */
    public function findById($id, $columns = ['*']);

    /**
     * Find data by field and value
     *
     * @param       $field
     * @param       $value
     * @param array $columns
     *
     * @return mixed
     */
    public function findByField($field, $value, $columns = ['*']);

    /**
     * Find data by multiple fields
     *
     * @param array $where
     * @param array $columns
     *
     * @return mixed
     */
    public function findWhere(array $where, $columns = ['*']);

    /**
     * Find data by multiple values in one field
     *
     * @param       $field
     * @param array $values
     * @param array $columns
     *
     * @return mixed
     */
    public function findWhereIn($field, array $values, $columns = ['*']);

    /**
     * Find data by excluding multiple values in one field
     *
     * @param       $field
     * @param array $values
     * @param array $columns
     *
     * @return mixed
     */
    public function findWhereNotIn($field, array $values, $columns = ['*']);

    /**
     * Update an entity in repository by id
     *
     * @param array $attributes
     * @param       $id
     *
     * @return mixed
     */
    public function updateRevise(array $attributes, $id);

    /**
     * Update or Create an entity in repository
     *
     * @param array $attributes
     * @param array $values
     *
     * @return mixed
     */
    public function updateOrCreate(array $attributes, array $values = []);

    /**
     * Return First or Create an entity in repository
     *
     * @param array $attributes
     * @param array $values
     *
     * @return mixed
     */
    public function firstOrCreate(array $attributes, array $values = []);

    /**
     * // Retrieve by name, or instantiate...
     *
     * @param array $attributes
     *
     * @return mixed
     */
    public function firstOrNew(array $attributes);

    /**
     * // Create new
     *
     * @param array $attributes
     *
     * @return mixed
     */
    public function createNew(array $attributes);

    /**
     * Retrieve all data of repository, paginated
     *
     * @param null $limit
     * @param array $columns
     * @param string $method
     *
     * @return mixed
     */
    public function paginate($limit = null, $columns = ['*'], $method = "paginate");

    /**
     * Retrieve all data of repository, simple paginated
     *
     * @param null $limit
     * @param array $columns
     *
     * @return mixed
     */
    public function simplePaginate($limit = null, $columns = ['*']);


    /**
     * Order collection by a given column
     *
     * @param string $column
     * @param string $direction
     *
     * @return $this
     */
    public function orderBy($column, $direction = 'asc');
    /**
     * Load relations
     *
     * @param $relations
     *
     * @return $this
     */
    public function with($relations);

    /**
     * Load relation with closure
     *
     * @param string $relation
     * @param closure $closure
     *
     * @return $this
     */
    public function whereHas($relation, $closure);

    /**
     * Add subselect queries to count the relations.
     *
     * @param  mixed $relations
     * @return $this
     */
    public function withCount($relations);
    /**
     * Set hidden fields
     *
     * @param array $fields
     *
     * @return $this
     */
    public function hidden(array $fields);
    /**
     * Set visible fields
     *
     * @param array $fields
     *
     * @return $this
     */
    public function visible(array $fields);
    /**
     * Query Scope
     *
     * @param \Closure $scope
     *
     * @return $this
     */
    public function scopeQuery(\Closure $scope);
    /**
     * Reset Query Scope
     *
     * @return $this
     */
    public function resetScope();


    /**
     * Set Presenter
     *
     * @param $presenter
     *
     * @return mixed
     */
    public function setPresenter($presenter);
    /**
     * Skip Presenter Wrapper
     *
     * @param bool $status
     *
     * @return $this
     */
    public function skipPresenter($status = true);


    /**
     * Sync relations
     *
     * @param $id
     * @param $relation
     * @param $attributes
     * @param bool $detaching
     * @return mixed
     */
    public function sync($id, $relation, $attributes, $detaching = true);
    /**
     * SyncWithoutDetaching
     *
     * @param $id
     * @param $relation
     * @param $attributes
     * @return mixed
     */
    public function syncWithoutDetaching($id, $relation, $attributes);


- MyOverrides\RepositoryContracts\RepositoryCriteriaInterface

::

    /**
     * Push Criteria for filter the query
     *
     * @param $criteria
     *
     * @return $this
     */
    public function pushCriteria($criteria);

    /**
     * Pop Criteria
     *
     * @param $criteria
     *
     * @return $this
     */
    public function popCriteria($criteria);

    /**
     * Get Collection of Criteria
     *
     * @return Collection
     */
    public function getCriteria();

    /**
     * Find data by Criteria
     *
     * @param CriteriaInterface $criteria
     *
     * @return mixed
     */
    public function getByCriteria(CriteriaInterface $criteria);

    /**
     * Skip Criteria
     *
     * @param bool $status
     *
     * @return $this
     */
    public function skipCriteria($status = true);

    /**
     * Reset all Criterias
     *
     * @return $this
     */
    public function resetCriteria();


Example Model
-------------
::

    namespace Core\Demo\Models;

    use Illuminate\Database\Eloquent\Model;
    use Cartalyst\Attributes\EntityInterface;
    use Platform\Attributes\Traits\EntityTrait;
    use Cartalyst\Support\Traits\NamespacedEntityTrait;

    use Prettus\Repository\Contracts\Presentable;
    use Prettus\Repository\Traits\PresentableTrait;
    use Prettus\Repository\Contracts\Transformable;

    class Demoproduct extends Model implements EntityInterface, Transformable, Presentable
    {
        use EntityTrait, NamespacedEntityTrait;
        use PresentableTrait;


        /**
         * {@inheritDoc}
         */
        use \MyOverrides\Traits\UuidForKey;
        public $incrementing = false;

        /**
         * {@inheritDoc}
         */
        protected $table = 'demoproducts';

        /**
         * {@inheritDoc}
         */
        protected $guarded = [
            'id',
        ];

        /**
         * {@inheritDoc}
         */
        protected $with = [
            'values.attribute',
        ];

        /**
         * {@inheritDoc}
         */
        protected static $entityNamespace = 'core/demo.demoproduct';


        /**
        * @return array
        */
        public function transform()
        {
            return [

                /* place your other model properties here */

                'id' =>  $this->id,
                'name' =>  $this->name,
                'color' =>  $this->color,
                'brand' =>  $this->brand,
                'category' =>  $this->category,
                'model' =>  $this->model,
                'weight' => (float) $this->weight,
                'height' => (float) $this->height,
                'width' => (float) $this->width,
                'created_at' => $this->created_at,
                'updated_at' => $this->updated_at,
            ];
        }
    }

Example Repository
------------------
::

    namespace Core\Demo\Repositories\Demoproduct;

    use Cartalyst\Support\Traits;
    use Symfony\Component\Finder\Finder;
    use Illuminate\Contracts\Container\Container;
    use  MyOverrides\Repository\Eloquent\BaseRepository;

    class DemoproductRepository extends BaseRepository implements DemoproductRepositoryInterface
    {
        use Traits\ContainerTrait, Traits\EventTrait, Traits\RepositoryTrait, Traits\ValidatorTrait;

        /**
         * The Data handler.
         *
         * @var \Core\Demo\Handlers\Demoproduct\DemoproductDataHandlerInterface
         */
        protected $data;

        /**
         * The Eloquent demo model.
         *
         * @var string
         */
        protected $model;

        /**
         * Constructor.
         *
         * @param  \Illuminate\Contracts\Container\Container  $app
         * @return void
         */
        public function __construct(Container $app)
        {
            $this->setContainer($app);

            $this->setDispatcher($app['events']);

            $this->data = $app['core.demo.demoproduct.handler.data'];

            $this->setValidator($app['core.demo.demoproduct.validator']);

            $this->setModel(get_class($app['Core\Demo\Models\Demoproduct']));

            parent::__construct($app);
        }
    }


Example api controller
----------------------
::

    namespace Core\Demo\Controllers\Api;

    use Core\Demo\Criteria\Demopost;
    use Psr\Http\Message\ServerRequestInterface;
    use Core\Demo\Repositories\Demopost\DemopostRepositoryInterface;
    use MyOverrides\Controllers\Api\BaseController as ApiBaseController;
    use CoreAuthentication;

    /**
     * @SWG\Resource(
     *   apiVersion="1.0.0",
     *   swaggerVersion="1.2",
     *   resourcePath="/Demoposts",
     *   description="Demoposts Api",
     *   produces="['application/json']"
     * )
     */
    class DemopostsController extends ApiBaseController
    {
        /**
         *
         * @param ServerRequestInterface $request
         *
         */
        public $request;

        /**
         *
         * @param array $parsedBody
         *
         */
        public $parsedBody;


        /**
         * Default CoreAuthentication adapter
         *
         * @param CoreAuthentication $coreAuthentication
         *
         */
        protected $coreAuthentication;


        /**
         * The Demo repository.
         *
         * @var \Core\Demo\Repositories\Demopost\DemopostRepositoryInterface
         */
        protected $demoposts;


        /**
         * Constructor.
         *
         * @param  \Core\Demo\Repositories\Demopost\DemopostRepositoryInterface $demoposts
         * @return void
         */
        public function __construct(
            ServerRequestInterface $request,
            DemopostRepositoryInterface $demoposts)
        {
            $this->request = $request;
            $this->parsedBody = $request->getParsedBody();

            $this->demoposts = $demoposts;

            $this->coreAuthentication = CoreAuthentication::local();

            $this->middleware('passport:api');
        }


        /**
         * @SWG\Model(id="index")
         * @SWG\Api(
         *   path="/api/demo/demoposts",
         *   @SWG\Operation(
         *      method="GET",
         *      summary="Get Demoposts",
         *      nickname="getDemoposts",
         *      @SWG\ResponseMessage(code=200, message="OK"),
         *      @SWG\ResponseMessage(code=400, message="Invalid request params"),
         *      @SWG\ResponseMessage(code=401, message="Caller is not authenticated"),
         *      @SWG\ResponseMessage(code=404, message="Resource not found")
         *   )
         * )
         */
        public function index()
        {
            $this->demoposts->pushCriteria(new Demopost());
            $demopost = $this->demoposts->paginate();
            return $this->respondWithPaging($demopost);
        }


        /**
         * @SWG\Model(id="store")
         * @SWG\Property(name="content", type="string", required=true, defaultValue="Bo"),
         * @SWG\Api(
         *   path="/api/demo/demoposts",
         *   @SWG\Operation(
         *      method="POST",
         *      summary="store demopost",
         *      nickname="storedemopost",
         *      @SWG\Parameter(name="body", description="Request body", required=true, type="store", paramType="body", allowMultiple=false),
         *      @SWG\ResponseMessage(code=200, message="OK"),
         *      @SWG\ResponseMessage(code=400, message="Invalid request params"),
         *      @SWG\ResponseMessage(code=401, message="Caller is not authenticated"),
         *      @SWG\ResponseMessage(code=404, message="Resource not found")
         *   )
         * )
         */
        public function store()
        {
            $input = $this->parsedBody;
            $input['userid'] = $this->coreAuthentication->getCurrentUser()->id;
            $demopost = $this->demoposts->createNew($input);
            return $this->respondWithSuccess($demopost);
        }

        /**
         * @SWG\Model(id="show")
         * @SWG\Api(
         *   path="/api/demo/demoposts/{id}",
         *   @SWG\Operation(
         *      method="GET",
         *      summary="show demopost",
         *      nickname="showdemopost",
         *      @SWG\Parameter(name="id", description="id", required=true, type="string", paramType="path", allowMultiple=false),
         *      @SWG\ResponseMessage(code=200, message="OK"),
         *      @SWG\ResponseMessage(code=400, message="Invalid request params"),
         *      @SWG\ResponseMessage(code=401, message="Caller is not authenticated"),
         *      @SWG\ResponseMessage(code=404, message="Resource not found")
         *   )
         * )
         */
        public function show($id)
        {
            $demopost = $this->demoposts->findById($id);
            if (empty($demopost)) {
                return $this->errorNotFound();
            }
            return $this->respondWithSuccess($demopost);
        }


        /**
         * @SWG\Model(id="update")
         * @SWG\Property(name="content", type="string", required=true, defaultValue="Bo"),
         * @SWG\Api(
         *   path="/api/demo/demoposts/{id}",
         *   @SWG\Operation(
         *      method="PUT",
         *      summary="update demopost",
         *      nickname="updatedemopost",
         *      @SWG\Parameter(name="id", description="id", required=true, type="string", paramType="path", allowMultiple=false),
         *      @SWG\Parameter(name="body", description="Request body", required=true, type="update", paramType="body", allowMultiple=false),
         *      @SWG\ResponseMessage(code=200, message="OK"),
         *      @SWG\ResponseMessage(code=400, message="Invalid request params"),
         *      @SWG\ResponseMessage(code=401, message="Caller is not authenticated"),
         *      @SWG\ResponseMessage(code=404, message="Resource not found")
         *   )
         * )
         */
        public function update($id)
        {
            $input = $this->parsedBody;
            $input['userid'] = $this->coreAuthentication->getCurrentUser()->id;

            $demopost = $this->demoposts->skipPresenter()->findById($id);
            if (empty($demopost) || $demopost->userid !== $input['userid']) {
                return $this->errorNotFound();
            }
            $demopost->fill($input);
            if ($demopost->save()) {
                return $this->respondWithSuccess($this->demoposts->skipPresenter(false)->parserResult($demopost));
            }
            $errorKey = '';
            return $this->respondWithErrorKey($errorKey);
        }

        /**
         * @SWG\Model(id="delete")
         * @SWG\Api(
         *   path="/api/demo/demoposts/{id}",
         *   @SWG\Operation(
         *      method="DELETE",
         *      summary="delete demopost",
         *      nickname="deletedemopost",
         *      @SWG\Parameter(name="id", description="id", required=true, type="string", paramType="path", allowMultiple=false),
         *      @SWG\ResponseMessage(code=200, message="OK"),
         *      @SWG\ResponseMessage(code=400, message="Invalid request params"),
         *      @SWG\ResponseMessage(code=401, message="Caller is not authenticated"),
         *      @SWG\ResponseMessage(code=404, message="Resource not found")
         *   )
         * )
         */
        public function delete($id)
        {
            $userId = $this->coreAuthentication->getCurrentUser()->id;
            $demopost = $this->demoposts->skipPresenter()->findById($id);
            if (empty($demopost) || $demopost->userid !== $userId) {
                return $this->errorNotFound();
            }
            if ($demopost->delete()) {
                return $this->respondWithSuccess($this->demoposts->skipPresenter(false)->parserResult($demopost));
            }
            $errorKey = '';
            return $this->respondWithErrorKey($errorKey);
        }
    }


Skip Presenter defined in the repository
----------------------------------------
Use skipPresenter before any other chaining method::

    $posts = $this->repository->skipPresenter()->all();

or::

    $this->repository->skipPresenter();

    $posts = $this->repository->all();

Use default transform in Eloquent Model
---------------------------------------
- Example::

    <?php

    namespace Core\Demo\Models;

    use Illuminate\Database\Eloquent\Model;
    use Cartalyst\Attributes\EntityInterface;
    use Platform\Attributes\Traits\EntityTrait;
    use Cartalyst\Support\Traits\NamespacedEntityTrait;

    use Prettus\Repository\Contracts\Presentable;
    use Prettus\Repository\Traits\PresentableTrait;
    use Prettus\Repository\Contracts\Transformable;

    class Demoproduct extends Model implements EntityInterface, Transformable, Presentable
    {
        use EntityTrait, NamespacedEntityTrait;
        use PresentableTrait;

        /**
        * @return array
        */
        public function transform()
        {
            return [

                /* place your other model properties here */

                'id' =>  $this->id,
                'name' =>  $this->name,
                'color' =>  $this->color,
                'brand' =>  $this->brand,
                'category' =>  $this->category,
                'model' =>  $this->model,
                'weight' => (float) $this->weight,
                'height' => (float) $this->height,
                'width' => (float) $this->width,
                'created_at' => $this->created_at,
                'updated_at' => $this->updated_at,
            ];
        }
    }



- Api response
.. image:: figure-1.png


Use other presenter, without default transform in Eloquent Model
----------------------------------------------------------------
Example::

    $this->postRepository->setPresenter(\Core\Demo\Presenters\Demopost::class);

* What is the \Core\Demo\Presenters\Demopost look like ?::

    <?php

    namespace Core\Demo\Presenters;

    use Core\Demo\Transformers\Demopost as DemopostTransformer;
    use Prettus\Repository\Presenter\FractalPresenter;

    class Demopost extends FractalPresenter
    {
         /**
         * Transformer
         *
         * @return \League\Fractal\TransformerAbstract
         */
        public function getTransformer()
        {
            return new DemopostTransformer();
        }

    }


* What is the \Core\Demo\Transformers\Demopost look like ?::

    <?php

    namespace Core\Demo\Transformers;

    use Core\Demo\Models\Demopost as DemopostModel;
    use League\Fractal\TransformerAbstract;

    class Demopost extends TransformerAbstract
    {

        /**
         * Transform the Demopost entity
         * @param DemopostModel $model
         *
         * @return array
         */
        public function transform(DemopostModel $model)
        {
            return [

                /* place your other model properties here */

                'id'         => $this->id,
                'title'      => $this->title,
                'created_at' => $model->created_at,
                'updated_at' => $model->updated_at
            ];
        }

    }


Use multiple criteria
---------------------
Example::

    /**
     * @SWG\Model(id="index")
     * @SWG\Api(
     *   path="/api/demo/demoposts",
     *   @SWG\Operation(
     *      method="GET",
     *      summary="Get Demoposts",
     *      nickname="getDemoposts",
     *      @SWG\ResponseMessage(code=200, message="OK"),
     *      @SWG\ResponseMessage(code=400, message="Invalid request params"),
     *      @SWG\ResponseMessage(code=401, message="Caller is not authenticated"),
     *      @SWG\ResponseMessage(code=404, message="Resource not found")
     *   )
     * )
     */
    public function index()
    {
        $this->demoposts->pushCriteria(new \Core\Demo\Criteria\Demopost());
        $this->demoposts->pushCriteria(new \Core\Demo\Criteria\OtherDemopost());
        $demopost = $this->demoposts->paginate();
        return $this->respondWithPaging($demopost);
    }

* What is the \Core\Demo\Criteria\Demopost look like ?::

    <?php

    namespace Core\Demo\Criteria;

    use MyOverrides\Repository\Contracts\RepositoryInterface;
    use MyOverrides\Repository\Contracts\CriteriaInterface;
    use CoreAuthentication;

    class Demopost implements CriteriaInterface
    {

        /**
         * Apply criteria in query repository
         *
         * @param \Core\Demo\Models\Demopost $model
         * @param RepositoryInterface $repository
         *
         * @return mixed
         */
        public function apply($model, RepositoryInterface $repository)
        {
            $userId = CoreAuthentication::local()->getCurrentUser()->id;
            return $model->where('userid', $userId);
        }
    }