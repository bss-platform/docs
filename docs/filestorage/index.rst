Filesystem / Cloud Storage
==========================

.. toctree::
    :maxdepth: 4
    :glob:

    intro
    configuration
    obtaining-disk-instances
    storing-files
    retrieving-files
    deleting-files
    directories


