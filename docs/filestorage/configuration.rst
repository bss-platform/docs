Configuration
=============

The filesystem configuration file is located at ``config/filesystems.php``.
Within this file you may configure all of your "disks".
Each disk represents a particular storage driver and storage location.
Example configurations for each supported driver are included in the configuration file.
So, simply modify the configuration to reflect your storage preferences and credentials.

Of course, you may configure as many disks as you like, and may even have multiple disks that use the same driver.


The Public Disk
---------------

The public disk is intended for files that are going to be publicly accessible.
 By default, the public disk uses the local driver and stores these files in ``storage/app/public``.
 To make them accessible from the web, you should create a symbolic link from ``public/storage to storage/app/public``.
 This convention will keep your publicly accessible files in one directory that can be easily shared across deployments when using zero down-time deployment systems like Envoyer.

To create the symbolic link, you may use the ``storage:link`` Artisan command:
::

    php artisan storage:link

Of course, once a file has been stored and the symbolic link has been created, you can create a URL to the files using the asset helper:
::

    echo asset('storage/file.txt');


The Local Driver
----------------

When using the local driver, all file operations are relative to the root directory defined in your configuration file.
By default, this value is set to the storage/app directory.
Therefore, the following method would store a file in ``storage/app/file.txt``:
::

    Storage::disk('local')->put('file.txt', 'Contents');

Driver Prerequisites
--------------------

**Composer Packages**

Before using the S3 or Rackspace drivers, you will need to install the appropriate package via Composer:
::

    Amazon S3: league/flysystem-aws-s3-v3 ~1.0
    Rackspace: league/flysystem-rackspace ~1.0

**S3 Driver Configuration**

The ``S3`` driver configuration information is located in your ``config/filesystems.php`` configuration file.
This file contains an example configuration array for an ``S3`` driver. You are free to modify this array with your own ``S3`` configuration and credentials.

**FTP Driver Configuration**

Laravel's Flysystem integrations works great with FTP; however, a sample configuration is not included with the framework's default ``filesystems.php`` configuration file.
If you need to configure a FTP filesystem, you may use the example configuration below:
::

    'ftp' => [
        'driver'   => 'ftp',
        'host'     => 'ftp.example.com',
        'username' => 'your-username',
        'password' => 'your-password',

        // Optional FTP Settings...
        // 'port'     => 21,
        // 'root'     => '',
        // 'passive'  => true,
        // 'ssl'      => true,
        // 'timeout'  => 30,
    ],

**Rackspace Driver Configuration**

Laravel's Flysystem integrations works great with Rackspace; however, a sample configuration is not included with the framework's default ``filesystems.php`` configuration file.
If you need to configure a Rackspace filesystem, you may use the example configuration below:
::

    'rackspace' => [
        'driver'    => 'rackspace',
        'username'  => 'your-username',
        'key'       => 'your-key',
        'container' => 'your-container',
        'endpoint'  => 'https://identity.api.rackspacecloud.com/v2.0/',
        'region'    => 'IAD',
        'url_type'  => 'publicURL',
    ],


