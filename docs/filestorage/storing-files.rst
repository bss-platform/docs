Storing Files
=============

Put by streams
--------------

``Storage::put(<expect path to put>, <stream of file>)``

The put method may be used to store raw file contents on a disk.
You may also pass a PHP  resource to the put method, which will use ``Flysystem's`` underlying stream support.
Using streams is greatly recommended when dealing with **large files**:
::

    use Illuminate\Support\Facades\Storage;

    Storage::put('file.jpg', $contents);

    Storage::put('file.jpg', $resource);
    Automatic Streaming


Put by a file storage location
------------------------------
* $visibility : ``public``, ``private``

* Automatically generate a UUID for file name

``Storage::putFile(<expect path to put>, new File(<file storage location>),$visibility)``

* Manually specify a file name

``Storage::putFileAs(<expect path to put>, new File(<file storage location>),<name of file>,$visibility)``


If you would like Laravel to automatically manage **streaming** a given file to your storage location, you may use the ``putFile`` or ``putFileAs`` method.
This method accepts either a  ``Illuminate\Http\File`` or ``Illuminate\Http\UploadedFile`` instance and will automatically stream the file to your desire location:
::

    use Illuminate\Http\File;

    // Automatically generate a UUID for file name...
    Storage::putFile('photos', new File('/path/to/photo'));

    // Manually specify a file name...
    Storage::putFileAs('photos', new File('/path/to/photo'), 'photo.jpg');


There are a few important things to note about the ``putFile`` method.
Note that we only specified a directory name, not a file name.
By default, the putFile method will generate a UUID to serve as the file name.
The path to the file will be returned by the putFile method so you can store the path, including the generated file name, in your database.

The putFile and putFileAs methods also accept an argument to specify the "visibility" of the stored file.
This is particularly useful if you are storing the file on a cloud disk such as S3 and would like the file to be publicly accessible:
::

    Storage::putFile('photos', new File('/path/to/photo'), 'public');


Prepending & Appending To Files
-------------------------------
The prepend and append methods allow you to write to the beginning or end of a file:
::

    Storage::prepend('file.log', 'Prepended Text');

    Storage::append('file.log', 'Appended Text');

Copying & Moving Files
----------------------

The copy method may be used to copy an existing file to a new location on the disk, while the  move method may be used to rename or move an existing file to a new location:
::

    Storage::copy('old/file1.jpg', 'new/file1.jpg');

    Storage::move('old/file1.jpg', 'new/file1.jpg');

File Uploads
------------

* **Automatically generate a UUID for file name**
``$path = $request->file(<input file upload name>)->store(<expect path to store>)``
``$path = Storage::putFile(<expect path to store>, $request->file(<input file upload name>))``

In web applications, one of the most common use-cases for storing files is storing user uploaded files such as profile pictures, photos, and documents.
Laravel makes it very easy to store uploaded files using the store method on an uploaded file instance.
Simply call the store method with the path at which you wish to ``store`` the uploaded file:
::

    namespace App\Http\Controllers;

    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class UserAvatarController extends Controller
    {
        /**
         * Update the avatar for the user.
         *
         * @param  Request  $request
         * @return Response
         */
        public function update(Request $request)
        {
            $path = $request->file('avatar')->store('avatars');

            return $path;
        }
    }


There are a few important things to note about this example.
Note that we only specified a directory name, not a file name.
By default, the store method will generate a UUID to serve as the file name.
The path to the file will be returned by the store method so you can store the path, including the generated file name, in your database.

You may also call the putFile method on the Storage facade to perform the same file manipulation as the example above:
::

    $path = Storage::putFile('avatars', $request->file('avatar'));


* **Specifying A File Name**

``$path = $request->file(<input file upload name>)->storeAs(<input file upload name>, <expect storage file name>)``
``$path = Storage::putFileAs(<expect storage file name>, $request->file(<input file upload name>), <expect storage file name>)``

If you would not like a file name to be automatically assigned to your stored file, you may use the  ``storeAs`` method, which receives the path, the file name, and the (optional) disk as its arguments:
::

    $path = $request->file('avatar')->storeAs(
        'avatars', $request->user()->id
    );

Of course, you may also use the putFileAs method on the Storage facade, which will perform the same file manipulation as the example above:
::

    $path = Storage::putFileAs(
        'avatars', $request->file('avatar'), $request->user()->id
    );

* **Specifying A Disk**

``$path = $request->file(<input file upload name>)->store(<expect storage file name>, <disk name>)``


By default, this method will use your default disk.
If you would like to specify another disk, pass the disk name as the second argument to the store method:
::

    $path = $request->file('avatar')->store(
        'avatars/'.$request->user()->id, 's3'
    );


File Visibility
---------------

In Laravel's Flysystem integration, "visibility" is an abstraction of file permissions across multiple platforms.
Files may either be declared ``public`` or ``private``.
When a file is declared public, you are indicating that the file should generally be accessible to others.
For example, when using the S3 driver, you may retrieve URLs for public files.

You can set the visibility when setting the file via the put method:
::

    use Illuminate\Support\Facades\Storage;

    Storage::put('file.jpg', $contents, 'public');


If the file has already been stored, its visibility can be retrieved and set via the ``getVisibility`` and  ``setVisibility`` methods:
::

    $visibility = Storage::getVisibility('file.jpg');

    Storage::setVisibility('file.jpg', 'public')
