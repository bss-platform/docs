Directories
===========

Get All Files Within A Directory
--------------------------------

The files method returns an array of all of the files in a given directory.
If you would like to retrieve a list of all files within a given directory including all sub-directories, you may use the allFiles method:
::

    use Illuminate\Support\Facades\Storage;

    $files = Storage::files($directory);

    $files = Storage::allFiles($directory);


Get All Directories Within A Directory
--------------------------------------

The directories method returns an array of all the directories within a given directory.
Additionally, you may use the allDirectories method to get a list of all directories within a given directory and all of its sub-directories:
::

    $directories = Storage::directories($directory);

    // Recursive...
    $directories = Storage::allDirectories($directory);


Create A Directory
------------------

The makeDirectory method will create the given directory, including any needed sub-directories:
::

    Storage::makeDirectory($directory);

Delete A Directory
------------------
Finally, the deleteDirectory may be used to remove a directory and all of its files:
::

    Storage::deleteDirectory($directory);



