Obtaining Disk Instances
========================


The Storage facade may be used to interact with any of your configured disks.
For example, you may use the put method on the facade to store an avatar on the default disk.
If you call methods on the Storage facade without first calling the disk method, the method call will automatically be passed to the ``default`` disk:
::

    use Illuminate\Support\Facades\Storage;

    Storage:: ...;

If your applications interacts with multiple disks, you may use the disk method on the Storage facade to work with files on a particular disk:
::

    Storage::disk('s3')->...;
