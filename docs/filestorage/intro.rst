Introduction
============

Laravel provides a powerful filesystem abstraction thanks to the wonderful ``Flysystem`` PHP package by Frank de Jonge.
The Laravel Flysystem integration provides simple to use drivers for working with local ``filesystems``, ``Amazon S3``, and ``Rackspace Cloud Storage``.
Even better, it's amazingly simple to switch between these storage options as the API remains the same for each system.





References
----------
* https://laravel.com/docs/5.3/filesystem
