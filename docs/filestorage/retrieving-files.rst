Retrieving Files
================

The get method may be used to retrieve the contents of a file.
The raw string contents of the file will be returned by the method.
Remember, all file paths should be specified relative to the "root" location configured for the disk:
::

    $contents = Storage::get('file.jpg');

The exists method may be used to determine if a file exists on the disk:
::

    $exists = Storage::disk('s3')->exists('file.jpg');


File URLs
---------

When using the local or s3 drivers, you may use the url method to get the URL for the given file.
If you are using the local driver, this will typically just prepend ``/storage`` to the given path and return a relative URL to the file.
If you are using the s3 driver, the fully qualified remote URL will be returned:
::

    use Illuminate\Support\Facades\Storage;

    $url = Storage::url('file1.jpg');

Remember, if you are using the local driver, all files that should be publicly accessible should be placed in the ``storage/app/public`` directory.
Furthermore, you should create a symbolic link at ``public/storage`` which points to the ``storage/app/public`` directory.

File Metadata
-------------

In addition to reading and writing files, Laravel can also provide information about the files themselves.
For example, the size method may be used to get the size of the file in bytes:
::

    use Illuminate\Support\Facades\Storage;

    $size = Storage::size('file1.jpg');

The lastModified method returns the UNIX timestamp of the last time the file was modified:
::

    $time = Storage::lastModified('file1.jpg');



