Deleting Files
==============

The delete method accepts a single filename or an array of files to remove from the disk:
::

    use Illuminate\Support\Facades\Storage;

    Storage::delete('file.jpg');

    Storage::delete(['file1.jpg', 'file2.jpg']);
