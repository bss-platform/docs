Configuration Caching
=====================
To give your application a speed boost, you should cache all of your configuration files into a single file using the ``config:cache`` Artisan command. This will combine all of the configuration options for your application into a single file which will be loaded quickly by the framework.

You should typically run the php artisan config:cache command as part of your production deployment routine. The command should not be run during local development as configuration options will frequently need to be changed during the course of your application's development.



