Environment Configuration
=========================
It is often helpful to have different configuration values based on the environment the application is running in. For example, you may wish to use a different cache driver locally than you do on your production server.

To make this a cinch, Laravel utilizes the DotEnv PHP library by Vance Lucas. In a fresh Laravel installation, the root directory of your application will contain a ``.env.example`` file. If you install Laravel via Composer, this file will automatically be renamed to .env. Otherwise, you should rename the file manually.


Retrieving Environment Configuration
------------------------------------

All of the variables listed in this file will be loaded into the ``$_ENV`` PHP super-global when your application receives a request. However, you may use the env helper to retrieve values from these variables in your configuration files. In fact, if you review the Laravel configuration files, you will notice several of the options already using this helper::

    'debug' => env('APP_DEBUG', false),

The second value passed to the env function is the "default value". This value will be used if no environment variable exists for the given key.

Your ``.env`` file should not be committed to your application's source control, since each developer / server using your application could require a different environment configuration.

If you are developing with a team, you may wish to continue including a ``.env.example`` file with your application. By putting place-holder values in the example configuration file, other developers on your team can clearly see which environment variables are needed to run your application.

Determining The Current Environment
-----------------------------------

The current application environment is determined via the ``APP_ENV`` variable from your ``.env`` file. You may access this value via the environment method on the App facade
::

    $environment = App::environment();

You may also pass arguments to the environment method to check if the environment matches a given value. The method will return true if the environment matches any of the given values
::

    if (App::environment('local')) {
        // The environment is local
    }

    if (App::environment('local', 'staging')) {
        // The environment is either local OR staging...
    }
