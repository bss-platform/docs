Configuration
=============
All of the configuration files for the Laravel framework are stored in the ``config`` directory.
Each option is documented, so feel free to look through the files and get familiar with the options available to you.



.. toctree::
    :maxdepth: 3

    environment-configuration
    accessing-configuration-values
    configuration-caching
    maintenance-mode